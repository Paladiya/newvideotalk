#include <jni.h>

extern "C" {

    // Auth key for server package checking

JNIEXPORT jstring JNICALL
Java_com_tech_rtcapp_MainActivity_getAPIKey(JNIEnv *env, jobject instance) {

    return env->NewStringUTF("com.livechat.onlinevideochat");
}

}