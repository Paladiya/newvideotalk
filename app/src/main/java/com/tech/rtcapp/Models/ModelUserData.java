package com.tech.rtcapp.Models;

import com.google.gson.annotations.SerializedName;

public class ModelUserData {

    @SerializedName("_id")
    String _id;
    @SerializedName("email")
    String email;
    @SerializedName("password")
    String password;
    @SerializedName("name")
    String name;
    @SerializedName("gender")
    String gender;
    @SerializedName("birthDate")
    String birthDate;
    @SerializedName("signupType")
    String signupType;
    @SerializedName("age")
    String age;
    @SerializedName("country")
    String country;
    @SerializedName("coin")
    int coin;
    @SerializedName("fcmToken")
    String fcmToken;
    @SerializedName("genderMatch")
    String genderMatch;
    @SerializedName("countryMatch")
    String countryMatch;
    @SerializedName("isAvailable")
    boolean isAvailable;

    String image;
    String JWT;

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getGenderMatch() {
        return genderMatch;
    }

    public void setGenderMatch(String genderMatch) {
        this.genderMatch = genderMatch;
    }

    public String getCountryMatch() {
        return countryMatch;
    }

    public void setCountryMatch(String countryMatch) {
        this.countryMatch = countryMatch;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }


    public String getJWT() {
        return JWT;
    }

    public void setJWT(String JWT) {
        this.JWT = JWT;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getSignupType() {
        return signupType;
    }

    public void setSignupType(String signupType) {
        this.signupType = signupType;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
