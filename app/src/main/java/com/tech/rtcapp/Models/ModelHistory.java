package com.tech.rtcapp.Models;

import com.google.gson.annotations.SerializedName;

//import io.realm.RealmObject;
//import io.realm.annotations.PrimaryKey;

public class ModelHistory {

//    @PrimaryKey
    @SerializedName("_id")
    String _id;
    @SerializedName("userId")
    String userId;
    @SerializedName("clientId")
    String clientId;
    @SerializedName("gender")
    String gender;
    @SerializedName("country")
    String country;
    @SerializedName("name")
    String name;
    @SerializedName("created")
    String created;
    @SerializedName("createdAt")
    String createdAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
