package com.tech.rtcapp.Models;

import com.google.gson.annotations.SerializedName;

public class ModelBuyCoins {

    @SerializedName("_id")
    String _id;
    @SerializedName("coins")
    int coins;
    @SerializedName("subId")
    String subId;
    @SerializedName("isBest")
    Boolean isBest;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public Boolean getBest() {
        return isBest;
    }

    public void setBest(Boolean best) {
        isBest = best;
    }
}
