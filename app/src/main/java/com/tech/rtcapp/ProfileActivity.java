package com.tech.rtcapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.Models.ModelUserData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    TextView TvName,TvCountry,TvId,TvEmpty;
    Toolbar toolbar;
    FloatingActionButton FlImage;
    ImageView IvProfile;
    SessionManager sessionManager;
    ModelUserData modelUserData;
    CollapsingToolbarLayout collapsingToolbar;
    List<Uri> mSelected;
    String path = Environment.getExternalStorageDirectory().toString()
            + "/Pictures/" + System.currentTimeMillis() + ".png";
    File imageFile;
    AlertDialog.Builder builder;
    String name;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_coordinator);
        apiInterface  = APIClient.getClient().create(APIInterface.class);

        sessionManager = new SessionManager(this);
        modelUserData = sessionManager.getUserDetails();

        collapsingToolbar = (CollapsingToolbarLayout)findViewById(R.id.collapsing);
//        collapsingToolbar.setTitle("Your Title");
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.transperent)); // transperent color = #00000000
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.solid_white)); //Color of your title
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        FlImage = (FloatingActionButton) findViewById(R.id.btn_profile);

        TvName = (TextView)findViewById(R.id.nameTextView);
        TvName.setText(modelUserData.getName());
        TvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGetName();
            }
        });


        TvCountry = (TextView)findViewById(R.id.locationInfoView);
        TvCountry.setText(modelUserData.getCountry());

        TvId = (TextView)findViewById(R.id.tv_id);
        TvId.setText(modelUserData.getEmail().substring(0,modelUserData.getEmail().indexOf("@")));

        IvProfile = (ImageView)findViewById(R.id.iv_profile);
        FlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
                startActivityForResult(intent, 1213);
            }
        });
        if (modelUserData.getImage() != null) {
            try {
                IvProfile.setImageURI(Uri.parse(modelUserData.getImage()));

            } catch (Exception e) {
                IvProfile.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));

            }
        } else {
            IvProfile.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));
        }

        if (modelUserData.getImage() != null) {
            try {
                FlImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera_normal));

            } catch (Exception e) {
                FlImage.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));

            }
        } else {
            FlImage.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));
        }



    }


    private void showGetName() {

        builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.inputNameTitle));
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(5,5,5,5);
        input.setLayoutParams(params);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name = input.getText().toString();
                if (name.equals(" ")){
                    Toast.makeText(ProfileActivity.this,getString(R.string.pls_entvalid_name),Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    return;
                }
                dialog.dismiss();
                Log.d("name",name);
                Utiles.ShowProgres(ProfileActivity.this,getString(R.string.plswaite));
                apiInterface.UpdateUserName(modelUserData.get_id(),name).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        Utiles.DismissProgress();
                        if (response.code()==200){
                            Toast.makeText(ProfileActivity.this,getString(R.string.name_update_suc),Toast.LENGTH_SHORT).show();
                            modelUserData.setName(name);
                            sessionManager.updateUser(modelUserData);
                            TvName.setText(name);
                        }else
                        {
                            Toast.makeText(ProfileActivity.this,getString(R.string.somethingwentwrong),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Utiles.DismissProgress();
                        Toast.makeText(ProfileActivity.this,getString(R.string.server_maintanance),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            IvProfile.setImageBitmap(selectedImage);
            OutputStream out = null;
            imageFile = new File(path);
            try {
                out = new FileOutputStream(imageFile);
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }

                } catch (Exception exc) {
                }
            }
            modelUserData.setImage(path);
            sessionManager.updateUser(modelUserData);
        }
    }
}
