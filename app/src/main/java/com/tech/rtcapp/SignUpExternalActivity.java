package com.tech.rtcapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.Models.ModelUserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpExternalActivity extends AppCompatActivity {

    TextView tv_date;
    RadioGroup gr_gender;
    Button btn_signup;
    SessionManager sessionManager;
    APIInterface apiInterface;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener date;
    ModelUserData userData;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_external);

        sessionManager = new SessionManager(this);
        userData = sessionManager.getUserDetails();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        tv_date = (TextView)findViewById(R.id.birthyear);
        gr_gender = (RadioGroup) findViewById(R.id.gender_group);
        btn_signup = (Button) findViewById(R.id.sign_up_button);

        calendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SignUpExternalActivity.this, date,1990, calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_date.getText().equals("")){
                    Toast.makeText(SignUpExternalActivity.this,getString(R.string.pls_sel_birthdate),Toast.LENGTH_SHORT).show();
                    return;
                }else if (gr_gender.getCheckedRadioButtonId()==0){
                    Toast.makeText(SignUpExternalActivity.this,getString(R.string.pls_sel_gender),Toast.LENGTH_SHORT).show();
                    return;
                }

                String gender = gr_gender.getCheckedRadioButtonId() == R.id.gender_male ? Constant.MALE : Constant.FEMALE;
                String birthDate = tv_date.getText().toString();
                Utiles.ShowProgres(SignUpExternalActivity.this,getString(R.string.plswaite));
                Log.d("data",gender+" "+birthDate);
                apiInterface.UpdateGender(userData.get_id(),gender,birthDate).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        Utiles.DismissProgress();
                        if (response.code() == 200){

                            try {
                                JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                                userData.setGender(jsonObject.getString("gender"));
                                userData.setBirthDate(jsonObject.getString("birthDate"));
                                sessionManager.updateUser(userData);
                                intent = new Intent(SignUpExternalActivity.this, SecondActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }else {

                            Toast.makeText(SignUpExternalActivity.this,response.message(),Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Utiles.DismissProgress();
                        Toast.makeText(SignUpExternalActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tv_date.setText(sdf.format(calendar.getTime()));
    }
}
