package com.tech.rtcapp;

public class Constant {

    public static final String Facebook = "Facebook";
    public static final String Google = "Google";
    public static final int REQUEST_CODE_ASK_ACCOUNT_PERMISSIONS = 100;
    public static String GLOBAL = "Global";
    public static int  GOOGLE_SIGNIN_CODE = 200;
    public static int  PERMISSION_LOCATION_CODE = 300;
    public static int  FACEBOOK_PERMISSION_CODE = 400;
    public static int  GOOGLE_PERMISSION_CODE = 500;
    public static int  GOOGLE_LOCATION_CODE = 600;
    public static int  REQUEST_CODE_CHOOSE_IMAGE = 700;
    public static String  User = "User";
    public static String  IsLogin = "IsLogin";
    public static String  MALE = "MALE";
    public static String  FEMALE = "FEMALE";
    public static String  BOTH = "Both";
    public static String  JWT = "JWT";
    public static String  ISPREMIUM = "ISPREMIUM";
    public static String  GENDER = "GENDER";
    public static String  COUNTRY = "COUNTRY";
    public static String  TOKEN = "token";
    public static String  SETTOKEN = "settoken";
    public static String  INITIALIZE = "INITIALIZE";
    public static String  ADD = "+";
    public static String  SUB = "-";
    public static String  Test = "test";
    public static String  NOAUTH = "NOAUTH";
    public static String  ISAVAILABLE = "isAvailable";
    public static String  SETAVAILABLE = "setAvailable";
    public static String  FROMUSER = "fromUser";
    public static String  TOUSER = "toUser";

    public static String  SETGENDER = "setgender";
    public static String SETCOUNTRY = "setcountry" ;

    public static String OnConnected = "OnConnected" ;

    public static String  UserId = "userId";
    public static String DoConnect = "doconnect";
    public static String DoConnectUser = "doconnectuser";
    public static String NoMatchingFound = "NoMatchingFounds";
    public static String NoMatchingFoundAgain = "NoMatchingFoundAgain";


    public static String app_link = "https://play.google.com/store/apps/details?id=";


    public static String Exit = "exit";
}
