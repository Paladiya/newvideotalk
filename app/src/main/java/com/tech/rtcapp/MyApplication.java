package com.tech.rtcapp;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.tech.rtcapp.API.SocketConnection;
import com.tech.rtcapp.Receivers.NetworkChangeReceiver;


import io.fabric.sdk.android.Fabric;

public class MyApplication extends MultiDexApplication {

    Utiles utiles;
    private static MyApplication sInstance;
//    private Billing  mBilling ;
    SessionManager sessionManager;
    static Context context;
    private BroadcastReceiver mNetworkReceiver;
    public BillingProcessor billingProcessor;


    public static Context getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
//        Realm.init(context);
//        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
//                .name("default.realm")
//                .schemaVersion(1)
//                .deleteRealmIfMigrationNeeded()
//                .build();
//
//        Realm.setDefaultConfiguration(realmConfiguration);
//        MultiDex.install(this);
        mNetworkReceiver = new NetworkChangeReceiver();

        Fabric.with(this, new Crashlytics());
        Log.d("MyApplication","called");
        AppEventsLogger.activateApp(this);
        FirebaseApp.initializeApp(getApplicationContext());
        utiles = new Utiles(this);
        sInstance = this;
//        mBilling = new Billing(this, new Billing.DefaultConfiguration() {
//            @Override
//            public String getPublicKey() {
//                return getString(R.string.billingProcessorKey);
//            }
//        });

        sessionManager = new SessionManager(this);
        if(!sessionManager.getPref(Constant.INITIALIZE).equals("true")){
            sessionManager.setPref(Constant.INITIALIZE,"true");
            sessionManager.setPref(Constant.COUNTRY,Constant.GLOBAL);
            sessionManager.setPref(Constant.GENDER,Constant.BOTH);
        }



    }

    public static MyApplication get() {
        return sInstance;
    }

//    public Billing getBilling() {
//        return mBilling;
//    }


//    public Billing getBilling() {
//        return mBilling;
//    }
}
