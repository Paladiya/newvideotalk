package com.tech.rtcapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.AssetDataSource;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.Models.ModelUserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import am.appwise.components.ni.NoInternetDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SignUpActivity";
    SimpleExoPlayer player;
    RelativeLayout btn_fb, btn_google;
    File imageFile;
    ModelUserData modelUserData;
    String newToken;
    String country, userName, email, picture, fbId, GAccountToken, photoUrl;
    String path = Environment.getExternalStorageDirectory().toString()
            + "/Pictures/" + System.currentTimeMillis() + ".png";
    APIInterface apiInterface;
    Intent intent;

    private AccessToken fbAccessToken;
    CallbackManager callbackManager;

    GoogleSignInOptions googleSignInOptions;
    GoogleSignInClient googleSignInClient;
    GoogleSignInAccount googleSignInAccount;
    GoogleApiClient googleApiClient;
    LocationManager locationManager;
    SessionManager sessionManager;
    Boolean Gclick = false;
    NoInternetDialog noInternetDialog;

    private boolean hasGPSDevice() {

        if (locationManager == null)
            return false;
        final List<String> providers = locationManager.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);
//        initializePlayer();

        noInternetDialog = new NoInternetDialog.Builder(this)
                .setBgGradientStart(getResources().getColor(R.color.colorPrimaryDarktoo)) // Start color for background gradient
                .setBgGradientCenter(getResources().getColor(R.color.colorPrimaryDark)) // Center color for background gradient
                .setBgGradientEnd(getResources().getColor(R.color.colorPrimary)) // End color for background gradient
                .setCancelable(false).build();

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                newToken = task.getResult().getToken();
                Log.d("fcmtoken",newToken);
            }
        });

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        sessionManager = new SessionManager(this);

        btn_fb = (RelativeLayout) findViewById(R.id.fb_login_button);
        btn_google = (RelativeLayout) findViewById(R.id.google_login_button);

        btn_fb.setOnClickListener(this);
        btn_google.setOnClickListener(this);

        fbAccessToken = AccessToken.getCurrentAccessToken();
        Log.d("FBAccessToken", String.valueOf(fbAccessToken));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                fbAccessToken = loginResult.getAccessToken();
                Log.d("onresponse", String.valueOf(fbAccessToken.getToken()));
                Log.d("onresponse", String.valueOf(AccessToken.getCurrentAccessToken()));
                getUserProfile(fbAccessToken);
            }

            @Override
            public void onCancel() {
                Log.d("onresponse", "cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("onresponse", "error");
            }
        });


        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.web_google_client_key))
//                .requestServerAuthCode(getString(R.string.web_google_client_key))
                .requestEmail()
                .requestProfile()
                .build();

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        googleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
//                    @Override
//                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//                        Log.d(TAG, "OnConnectionFailedListener");
//                    }
//                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(SignUpActivity.this);


        ((TextView) findViewById(R.id.sign_in_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(SignUpActivity.this, LoginActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        logOutFacebook();
        logOut();

//        logOutFacebook();
//        logOut();
        printHashKey(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
//                Log.i("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("requestCode", String.valueOf(requestCode));
        if (requestCode == Constant.GOOGLE_SIGNIN_CODE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            if (task.isSuccessful()) {
                try {
                    handleSignInResult(task);
                } catch (ApiException e1) {
                    e1.printStackTrace();
                }
            } else {
                Log.d(TAG, "onGoogleSignfailed " + task.getException());
            }
        } else if (requestCode == Constant.GOOGLE_LOCATION_CODE && Gclick) {
            Log.d("click", "gclick");
            if (Gclick) {
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                    onClick(btn_google);
                } else {
                    enableLoc();
                }
            } else {
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                    onClick(btn_fb);
                } else {
                    enableLoc();
                }
            }
        }

    }

    private void handleSignInResult(final Task<GoogleSignInAccount> task) throws ApiException {

        Utiles.ShowProgres(SignUpActivity.this, getString(R.string.register_new_account));

        GoogleSignInAccount account = task.getResult(ApiException.class);
        GAccountToken = account.getIdToken();

        Log.d("account", new Gson().toJson(account));

        email = account.getEmail();
        if (account.getDisplayName().equals("")) {
            if (account.getGivenName().equals("")) {
                userName = account.getFamilyName();
            } else {
                userName = account.getGivenName();
            }
        } else {
            userName = account.getDisplayName();
        }

        modelUserData = new ModelUserData();
        modelUserData.setFcmToken(newToken);
        modelUserData.setSignupType(Constant.Google);
        modelUserData.setName(userName);
        modelUserData.setPassword(GAccountToken);
        modelUserData.setEmail(email);
        modelUserData.setImage(path);
        if (country!=null){
            modelUserData.setCountry(country);
        }
        if(account.getPhotoUrl()!=null){
            photoUrl = account.getPhotoUrl().toString();
        }

        SaveImage(photoUrl);
        SignUp();

    }


    public void logOut() {
        googleSignInClient.signOut()
                .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("logout", "success");
                        }
                    }
                });
    }

    private void getUserProfile(final AccessToken mAccessToken) {

        Utiles.ShowProgres(this, getString(R.string.register_new_account));

        GraphRequest graphRequest = GraphRequest.newMeRequest(mAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                Log.d("data", new Gson().toJson(object));

                try {
                    fbId = object.getString("id");

                    modelUserData = new ModelUserData();
                    modelUserData.setEmail(object.getString("email"));
                    modelUserData.setPassword(mAccessToken.getToken());
                    modelUserData.setName(object.getString("name"));
                    modelUserData.setSignupType(Constant.Facebook);
                    modelUserData.setFcmToken(newToken);
                    modelUserData.setImage(path);

                    if (country!=null){
                        modelUserData.setCountry(country);
                    }
                    picture = object.getJSONObject("picture").getJSONObject("data").getString("url");
                    SaveImage(picture);
                    SignUp();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void SignUp() {
        apiInterface.SignUp(modelUserData).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Utiles.DismissProgress();
                try {
                    Log.d(TAG, new Gson().toJson(response.body()));
                    if (response.code() == 409) {

                        Toast.makeText(SignUpActivity.this, getString(R.string.email_allready_available), Toast.LENGTH_LONG).show();

                    } else if (response.code() == 200) {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        try {
                            modelUserData.setJWT(jsonObject.getString("token"));
                            modelUserData.set_id(jsonObject.getJSONObject("user").getString("_id"));
                            modelUserData.setCoin(jsonObject.getJSONObject("user").getInt("coin"));

                        } catch (Exception e) {
                            Log.e(TAG, "for Google Sign up error " + e.getMessage());
                        } finally {
                            sessionManager.setLogin(true);
                            sessionManager.updateUser(modelUserData);
                            if(!sessionManager.getPref(Constant.INITIALIZE).equals("true")){
                                sessionManager.setPref(Constant.INITIALIZE,"true");
                                sessionManager.setPref(Constant.COUNTRY,Constant.GLOBAL);
                                sessionManager.setPref(Constant.GENDER,Constant.BOTH);
                            }
                            intent = new Intent(SignUpActivity.this, SignUpExternalActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                    } else if (response.code() == 201) {

                        try {
                            JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                            modelUserData.setJWT(jsonObject.getString("token"));
                            modelUserData.set_id(jsonObject.getJSONObject("user").getString("_id"));
                            modelUserData.setCoin(jsonObject.getJSONObject("user").getInt("coin"));
                            modelUserData.setGender(jsonObject.getJSONObject("user").getString("gender"));
                            modelUserData.setBirthDate(jsonObject.getJSONObject("user").getString("birthDate"));

                            sessionManager.setLogin(true);
                            sessionManager.updateUser(modelUserData);
                            if(!sessionManager.getPref(Constant.INITIALIZE).equals("true")){
                                sessionManager.setPref(Constant.INITIALIZE,"true");
                                sessionManager.setPref(Constant.COUNTRY,Constant.GLOBAL);
                                sessionManager.setPref(Constant.GENDER,Constant.BOTH);
                            }
                            intent = new Intent(SignUpActivity.this, SecondActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(SignUpActivity.this, getString(R.string.somethingwentwrong), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Utiles.DismissProgress();
//                        Toast.makeText(SignUpActivity.this,getString(R.string.server_maintanance),Toast.LENGTH_LONG).show();
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void SaveImage(String photoUrl) {
        Glide.with(SignUpActivity.this)
                .load(photoUrl)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource,
                                                @Nullable Transition<? super Drawable> transition) {
                        Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                        OutputStream out = null;
                        imageFile = new File(path);
                        try {
                            out = new FileOutputStream(imageFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                            out.flush();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (out != null) {
                                    out.close();
                                }

                            } catch (Exception exc) {
                            }
                        }

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utiles.isInternetOn(this))
        {
            noInternetDialog.show();
            Log.d("nointernet","yes");
            return;
        }
//        player.seekTo(0);
        Log.d(TAG, "onresume");
    }

    private void initializePlayer() {
        if (player == null) {

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);

            player = ExoPlayerFactory.newSimpleInstance(
                    this,
                    trackSelector);

            SimpleExoPlayerView simpleExoPlayerView = findViewById(R.id.player_bg_signup);
            simpleExoPlayerView.setPlayer(player);
            DataSpec dataSpec = new DataSpec(Uri.parse("asset:///" + "intro" + ".mp4"));
            final AssetDataSource assetDataSource = new AssetDataSource(this);
            try {
                assetDataSource.open(dataSpec);
            } catch (AssetDataSource.AssetDataSourceException e) {
                e.printStackTrace();
            }

            DataSource.Factory factory = new DataSource.Factory() {
                @Override
                public DataSource createDataSource() {
                    //return rawResourceDataSource;
                    return assetDataSource;
                }
            };

            MediaSource audioSource = new ExtractorMediaSource(assetDataSource.getUri(),
                    factory, new DefaultExtractorsFactory(), null, null);
            player.prepare(audioSource);
            player.setRepeatMode(Player.REPEAT_MODE_ALL);
            player.setPlayWhenReady(true);
        }

    }

    public void logOutFacebook() {
        LoginManager.getInstance().logOut();
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.fb_login_button:

                Gclick = false;

                if (!checkPermissonsGoogle(Constant.FACEBOOK_PERMISSION_CODE)) {

                    return;
                }

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                    Toast.makeText(SignUpActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
                    enableLoc();
                    return;
                } else {
                    getLocation();
                }

                if (fbAccessToken != null && !fbAccessToken.isExpired()) {
                    logOutFacebook();
                }
                LoginManager.getInstance().logInWithReadPermissions(SignUpActivity.this, Arrays.asList("public_profile", "email"));

                break;

            case R.id.google_login_button:

                Gclick = true;

                if (!checkPermissonsGoogle(Constant.GOOGLE_PERMISSION_CODE)) {

                    return;
                }

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                    Toast.makeText(SignUpActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
                    enableLoc();
                    return;
                } else {
                    getLocation();
                }

                if (googleSignInAccount != null) {
                    logOut();
                }

                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, Constant.GOOGLE_SIGNIN_CODE);

                break;
        }
    }

    private boolean checkPermissons(int PermissionCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                        PermissionCode);
                return false;
            }
            return true;
        }
        return true;
    }

    private boolean checkPermissonsGoogle(int PermissionCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        PermissionCode);
                return false;
            }
            return true;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean allGranted = false;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                allGranted = true;
            } else {
                allGranted = false;
            }
        }
        Log.d("all", String.valueOf(allGranted));
        if (allGranted && requestCode == Constant.FACEBOOK_PERMISSION_CODE) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                onClick(btn_fb);
            } else {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                    Log.e("keshav", "Gps already enabled");
                    Toast.makeText(SignUpActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
                    enableLoc();
                } else {
                    onClick(btn_google);
                    Log.e("keshav", "Gps already enabled");
                    Toast.makeText(SignUpActivity.this, "Gps already enabled", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (allGranted && requestCode == Constant.GOOGLE_PERMISSION_CODE) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                onClick(btn_google);
            } else {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
                    Log.e("keshav", "Gps already enabled");
                    Toast.makeText(SignUpActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
                    enableLoc();
                } else {
                    onClick(btn_google);
                    Log.e("keshav", "Gps already enabled");
                    Toast.makeText(SignUpActivity.this, "Gps already enabled", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void getLocation() {

        Log.d("get location", "click");

        SingleShotLocationProvider.requestSingleUpdate(SignUpActivity.this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Geocoder gcd = new Geocoder(SignUpActivity.this, Locale.getDefault());
                        List<Address> addresses;
                        try {
                            addresses = gcd.getFromLocation(location.latitude, location.longitude, 1);
                            country = addresses.get(0).getCountryName();
                            Log.d("Location", "my location is " + country);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    private void enableLoc() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        googleApiClient.connect();
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                    }
                })
                .build();

        googleApiClient.connect();

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        final LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest).setAlwaysShow(true).build();

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest);
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes
                            .RESOLUTION_REQUIRED: {
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SignUpActivity.this, Constant.GOOGLE_LOCATION_CODE);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    }
                }
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();

    }
}
