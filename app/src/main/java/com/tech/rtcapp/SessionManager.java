package com.tech.rtcapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tech.rtcapp.Models.ModelUserData;

public class SessionManager {

    Context _context;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    private static String PREF_NAME ;
    int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this._context = context;
        PREF_NAME = context.getString(R.string.app_name);
        this.pref = this._context.getSharedPreferences(PREF_NAME, this.PRIVATE_MODE);
        this.editor = this.pref.edit();
    }

    public void updateUser(ModelUserData ModelUserData) {
        this.editor.putString(Constant.User,new Gson().toJson(ModelUserData));
        this.editor.commit();
    }

    public void setPref(String key, String value){
        this.editor.putString(key,value);
        this.editor.commit();
    }

    public String getPref(String key){
        return pref.getString(key," ");
    }

    public boolean isLoggedIn() {
        return this.pref.getBoolean(Constant.IsLogin, false);
    }

    public void setLogin(Boolean islogin){this.editor.putBoolean(Constant.IsLogin,islogin);
        this.editor.commit();
    }

    public ModelUserData getUserDetails() {
        ModelUserData ModelUserData = new ModelUserData();
        ModelUserData = new Gson().fromJson(this.pref.getString(Constant.User,null), ModelUserData.class);
        return ModelUserData;
    }

    public void logoutUser() {
        this.editor.clear();
        this.editor.commit();
        Intent i = new Intent(this._context, LoginActivity.class);
        i.addFlags(67108864);
        this._context.startActivity(i);
    }

    public void clearAll(){
        pref.edit().clear().apply();
    }
    
}
