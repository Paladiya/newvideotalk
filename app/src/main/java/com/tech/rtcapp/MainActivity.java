package com.tech.rtcapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.gson.Gson;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.API.Links;
import com.tech.rtcapp.API.SocketConnection;
import com.tech.rtcapp.Models.ModelHistory;
import com.tech.rtcapp.Models.ModelUserData;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.DataChannel;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFileRenderer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
//import io.realm.Realm;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity   {

    private static final String VIDEO_TRACK_ID = "video1";
    private static final String AUDIO_TRACK_ID = "audio1";
    private static final String LOCAL_STREAM_ID = "stream1";
    private static final String SDP_MID = "sdpMid";
    private static final String SDP_M_LINE_INDEX = "sdpMLineIndex";
    private static final String SDP = "sdp";
    private static final String CREATEOFFER = "createoffer";
    private static final String OFFER = "offer";
    private static final String ANSWER = "answer";
    private static final String CANDIDATE = "candidate";
    private static final String DISCONNECT = "disconnect";
    private static final String Leave = "Leave";
    private static final String Connected = "connected";
    private static final String NOUSER = "nouser";


    private String To = "to";

    static {
        System.loadLibrary("native-lib");
    }
    Boolean isrunning = true;
    LinearLayout lin_callContainer;
    ImageButton  btn_callEnd, btn_report;
    ImageView btn_speaker, btn_mic, btn_camera;
    Boolean speaker = false;
    Boolean mics = false;
    VideoCapturer videoCapturerAndroid;
    VideoTrack localVideoTrack, getLocalVideoTrack;
    AudioTrack localAudioTrack;
    AudioManager audioManager;
    Boolean disconnect = false;
    Boolean connected = false;
    TextView tv_status;
    Chronometer chronometer;
    String encrypt;
    SurfaceViewRenderer localVideoView, remoteVideoView;
    FrameLayout frameLayout;
    SurfaceView gl_local_video, gl_other_video;
    MediaPlayer mediaPlayer;
    FrameLayout frameNouser;
    Camera mCamera;
    Boolean nouser = false, cameraBack = false;
    public PeerConnectionFactory peerConnectionFactory;
    private VideoSource localVideoSource;
    private PeerConnection peerConnection;
    private MediaStream localMediaStream;
    private VideoFileRenderer otherPeerRenderer, localPeerRenderer, newPeerRenderer;
    SessionManager sessionManager;
    ModelUserData modelUserData;
    String query;
    APIInterface apiInterface;
    MediaStream myMediaStream;
    boolean front = true;
    ArcProgress arcProgress;
    TextView TvNext;
    EglBase rootEglBase;
    MediaConstraints audioConstraints;
    MediaConstraints videoConstraints;
    MediaConstraints sdpConstraints;
    VideoSource videoSource;
    AudioSource audioSource;
    FrameLayout FramRoot;
//    Realm realm;
    ModelUserData fromUser;
    Intent intent;
    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();
//        VideoCapturer videoCapturer ;
//        if (front){
//            videoCapturer = enumerator.createCapturer("1", null);
//        }else {
//            videoCapturer = enumerator.createCapturer("0", null);
//        }

        // First, try to find front facing camera
        Logging.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating front facing camera capturer. "+deviceName);
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Logging.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    PeerConnection.Observer peerConnectionObserver = new PeerConnection.Observer()
    {
        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {
            Log.e("RTCAPP", "onSignalingChange:" + signalingState.toString());
        }


        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
            Log.e("RTCAPP", "onIceConnectionChange:" + iceConnectionState.toString());
            Log.e("RTCAPP", "To " + To);
            if (String.valueOf(iceConnectionState.toString()).equals("CONNECTED")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            updateVideoViews(true);
                            TvNext.setVisibility(View.VISIBLE);
                            arcProgress.setVisibility(View.GONE);
                            SimpleDateFormat outputFormat= new SimpleDateFormat("HH:mm dd/MM/yyyy");

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("To", To);
                            jsonObject.put("Time",outputFormat.format(new Date()));
                            connected = true;
                            btn_callEnd.setVisibility(View.VISIBLE);
                            SocketConnection.connect(MainActivity.this).emit(Connected, jsonObject);
                            tv_status.setVisibility(View.GONE);
                            chronometer.setVisibility(View.VISIBLE);
                            chronometer.setBase(SystemClock.elapsedRealtime());
                            chronometer.start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        apiInterface.GetUser(To).enqueue(new Callback<ModelUserData>() {
                            @Override
                            public void onResponse(Call<ModelUserData> call, Response<ModelUserData> response) {
                                    Log.d("user",new Gson().toJson(response.body()));
                                    ModelUserData myUser = response.body();
                                    if(myUser !=null){
                                        ModelHistory modelHistory = new ModelHistory();
                                        modelHistory.set_id(myUser.get_id());
                                        modelHistory.setUserId(modelUserData.get_id());
                                        modelHistory.setClientId(myUser.get_id());
                                        modelHistory.setCountry(myUser.getCountry()!=null?myUser.getCountry():" ");
                                        modelHistory.setGender(myUser.getGender());
                                        modelHistory.setName(myUser.getName());

                                        SimpleDateFormat outputFormat= new SimpleDateFormat("HH:mm dd/MM/yyyy");
                                        String currentDateandTime = outputFormat.format(new Date());
                                        Log.d("times",currentDateandTime);
                                        modelHistory.setCreatedAt(currentDateandTime);

//                                        realm.beginTransaction();
//                                        realm.insertOrUpdate(modelHistory);
//                                        realm.commitTransaction();
                                        RvProfile.setVisibility(View.VISIBLE);
                                        if (myUser.getCountry()!=null){
                                            TvCountryName.setVisibility(View.GONE);
                                        }else {
                                            TvCountryName.setText(myUser.getCountry());
                                        }
                                        TvUserName.setText(myUser.getName());
                                        IvProfile.setImageDrawable(myUser.getGender().equals(Constant.MALE)? getResources().getDrawable(R.drawable.male_img):getResources().getDrawable(R.drawable.female_img));
                                        if (myUser.getGender().equals(Constant.MALE))
                                        {
                                            img_gender.setImageDrawable(getResources().getDrawable(R.drawable.ic_select_male));
                                        }else {
                                            img_gender.setImageDrawable(getResources().getDrawable(R.drawable.ic_select_female));
                                        }
                                    }
                            }

                            @Override
                            public void onFailure(Call<ModelUserData> call, Throwable t) {

                            }
                        });

                    }
                });
            } else if (String.valueOf(iceConnectionState).equals("DISCONNECTED") && !Utiles.isInternetOn(MainActivity.this)) {
                disconnect = true;
                connected = false;
                Disconnect();
//                ReInitControl();
            }
        }

        @Override
        public void onIceConnectionReceivingChange(boolean b) {
            Log.e("RTCAPP", "onSignalingChange:" + String.valueOf(b));
        }

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
            Log.e("RTCAPP", "onGhatehringstatechage " + iceGatheringState.toString());

        }

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {
            Log.e("RTCAPP", "onIceCandidate " + String.valueOf(iceCandidate));
            try {
                JSONObject obj = new JSONObject();
                obj.put(SDP_MID, iceCandidate.sdpMid);
                obj.put(SDP_M_LINE_INDEX, iceCandidate.sdpMLineIndex);
                obj.put(SDP, iceCandidate.sdp);
                obj.put("To", To);
                SocketConnection.connect(MainActivity.this).emit(CANDIDATE, obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
            Log.e("RTCAPP", "onIceCandidate remove" + String.valueOf(iceCandidates));
        }

        @Override
        public void onAddStream(final MediaStream mediaStream) {

            if (!sessionManager.getPref(Constant.COUNTRY).equals(Constant.GLOBAL)) {
                Log.d("charge", "country");
                int coins = getResources().getInteger(R.integer.coins_for_location);
                modelUserData.setCoin(modelUserData.getCoin() >= getResources().getInteger(R.integer.required_coin) ? modelUserData.getCoin() - coins : 0);
                apiInterface.UpdateUserCoin(modelUserData.getJWT(), modelUserData.getCoin()).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.code() == 200) {
                            sessionManager.updateUser(modelUserData);
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });
            }
            if (!sessionManager.getPref(Constant.GENDER).equals(Constant.BOTH)) {
                Log.d("charge", "gender");
                int coins = getResources().getInteger(R.integer.coins_for_location);
                modelUserData.setCoin(modelUserData.getCoin() >= getResources().getInteger(R.integer.required_coin) ? modelUserData.getCoin() - coins : 0);
                apiInterface.UpdateUserCoin(modelUserData.getJWT(), modelUserData.getCoin()).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.code() == 200) {
                            sessionManager.updateUser(modelUserData);
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });
            }
            Log.e("RTCAPP", "onAddStream:");
            Log.e("RTCAPP ", "audio size" + String.valueOf(mediaStream.audioTracks));
            Log.e("RTCAPP ", "video size" + String.valueOf(mediaStream.videoTracks));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (mediaStream.audioTracks.size() > 1 || mediaStream.videoTracks.size() > 1) {
                                    Log.d("Weird-looking stream:", mediaStream.toString());
                                } else {

                                    runOnUiThread(() -> {
                                        final VideoTrack videoTrack = mediaStream.videoTracks.get(0);
                                        try {
                                            remoteVideoView.setVisibility(View.VISIBLE);
                                            videoTrack.addSink(remoteVideoView);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    });

//                                    localVideoTrack.setEnabled(false);
//                                    VideoCapturer.setView(remoteVideoView, null);
//                                    try {
//                                        newPeerRenderer = VideoRendererGui.createGui(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//                                        localPeerRenderer = VideoRendererGui.createGui(67, 67, 30, 30, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                    getLocalVideoTrack = mediaStream.videoTracks.get(0);
//                                    getLocalVideoTrack.setEnabled(true);
//                                    getLocalVideoTrack.addRenderer(newPeerRenderer);
//
//                                    localVideoTrack.setEnabled(true);
//                                    localVideoTrack.addRenderer(localPeerRenderer);
//                                    localVideoSource.restart();

//                                    localVideoTrack.setEnabled(true);
//                                    localVideoSource.restart();


                                }

                            } catch (Exception e) {
                                Log.d("onstop", "addstream");
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });

        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
            Log.e("RTCAPP", "onRemoveStream");
        }

        @Override
        public void onDataChannel(DataChannel dataChannel) {
            Log.e("RTCAPP", "onDatachannel");
        }

        @Override
        public void onRenegotiationNeeded() {
            Log.e("RTCAPP", "onRenagotiationNeeded");
        }

        @Override
        public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
            Log.e("RTCAPP", "onAddTrack " );
        }
    };
    private SurfaceHolder surfaceHolder_local, surfaceHolder_other, surfaceHolder = null;
    private boolean createOffer = false;
    SdpObserver sdpObserver = new SdpObserver() {

        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {

            Log.e("RTCAPP", "onSDPCreate4 Success");

            peerConnection.setLocalDescription(sdpObserver, sessionDescription);
            try {
                JSONObject obj = new JSONObject();
                obj.put(SDP, sessionDescription.description);
                obj.put("To", To);
                Log.e("to", To);
                if (createOffer) {
                    SocketConnection.connect(MainActivity.this).emit(OFFER, obj);
                } else {
                    SocketConnection.connect(MainActivity.this).emit(ANSWER, obj);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSetSuccess() {
            Log.e("RTCAPP", "onSDPSetCreateSuccess");

        }

        @Override
        public void onCreateFailure(String s) {
            Log.e("RTCAPP", "onSDPCreateFailure");

        }

        @Override
        public void onSetFailure(String s) {
            Log.e("RTCAPP", "onSDPSetFailer");

        }
    };

    public native String getAPIKey();

    String TAG;
    Dialog dialogExit;
    private TextView yes, no;
    ImageView img_male, img_female, img_both, img_gender;
    CircleImageView IvProfile;
    TextView TvUserName, TvCountryName;
    RelativeLayout RvProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        TAG = getClass().getSimpleName();

        sessionManager = new SessionManager(this);
        modelUserData = sessionManager.getUserDetails();
        apiInterface = APIClient.getClient().create(APIInterface.class);

//        realm = Realm.getDefaultInstance();

        SocketConnection.connect(MainActivity.this);

        lin_callContainer = (LinearLayout) findViewById(R.id.buttons_call_container);
        btn_speaker = (ImageView) findViewById(R.id.button_speaker);
        btn_camera = (ImageView) findViewById(R.id.button_call_switch_camera);
        btn_mic = (ImageView) findViewById(R.id.button_mic);
        btn_callEnd = (ImageButton) findViewById(R.id.button_call_disconnect);
        tv_status = (TextView) findViewById(R.id.tvstatus);
        chronometer = (Chronometer) findViewById(R.id.simpleChronometer);
        btn_report = (ImageButton) findViewById(R.id.img_report);
        gl_local_video = (SurfaceView) findViewById(R.id.small_video);
        gl_other_video = (SurfaceView) findViewById(R.id.big_video);
        frameNouser = (FrameLayout) findViewById(R.id.no_user);
        arcProgress = (ArcProgress) findViewById(R.id.arc_progress);
        TvNext = (TextView) findViewById(R.id.tv_next);
        localVideoView = findViewById(R.id.glview_call);
        remoteVideoView = findViewById(R.id.glview_call1);
        FramRoot = (FrameLayout) findViewById(R.id.fram_root);

        TvCountryName = (TextView) findViewById(R.id.tv_country);
        TvUserName = (TextView) findViewById(R.id.tv_username);
        IvProfile = (CircleImageView) findViewById(R.id.iv_profile);
        RvProfile = (RelativeLayout) findViewById(R.id.rel_profile);
        img_gender = (ImageView)findViewById(R.id.iv_gender);

        if (getIntent().getStringExtra("user")!=null){
            fromUser = new Gson().fromJson(getIntent().getStringExtra("user"),ModelUserData.class);
        }

        TvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connected) {

//                    onLeave();
//                    Disconnect();
//                    ReInitControl();
                }
            }
        });

        dialogExit = new Dialog(this, R.style.WideDialog);
        dialogExit.setContentView(R.layout.activity_exit);
        dialogExit.setCancelable(false);
        ((TextView) dialogExit.findViewById(R.id.tv_main)).setText(getString(R.string.areyousure_stop_matching));
        this.yes = (TextView) dialogExit.findViewById(R.id.btnYes);
        this.no = (TextView) dialogExit.findViewById(R.id.btnNo);
        this.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
                if (!isrunning) {
//                    Toast.makeText(MainActivity.this, getString(R.string.disconnect_toast), Toast.LENGTH_SHORT).show();
                    onLeave();
                    Disconnect();
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.stopafter2), Toast.LENGTH_SHORT).show();
                }
            }
        });
        this.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
            }
        });

        gl_other_video.setSecure(true);
        gl_local_video.getHolder().setFormat(PixelFormat.TRANSLUCENT);
        gl_local_video.setZOrderMediaOverlay(true);
        gl_local_video.setZOrderOnTop(true);


        btn_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIconAttribute(android.R.attr.alertDialogIcon);
                builder.setTitle(getString(R.string.app_name));
                builder.setMessage("Are you sure to report this user?");
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("RTCAPP dialog", "on nagative");
                    }
                });
                builder.setNeutralButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("RTCAPP dialog", "on neutral");
                        dialog.cancel();
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", getString(R.string.adminmain), null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.emailsubject));
                        emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.emailbody));
                        startActivity(Intent.createChooser(emailIntent, "Send email..."));
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Log.e("RTCAPP dialog", "on cancel");
                        dialog.cancel();
                    }
                });
                if (!isFinishing()) {
                    builder.create().show();
                }
            }
        });

        btn_callEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isrunning) {
//                    Toast.makeText(MainActivity.this, getString(R.string.diconnectA5), Toast.LENGTH_SHORT).show();
//                } else {
//                    disconnectCall();
//                }
                disconnectCall();

            }
        });

        btn_speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isrunning) {
//                    Toast.makeText(MainActivity.this, getString(R.string.diconnectA5), Toast.LENGTH_SHORT).show();
//                } else {
//                    toggleSpeaker();
//                }
                toggleSpeaker();

            }
        });

        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("RTCAPP", "onDiconnect");
//                if (isrunning)
//                {
//                    Toast.makeText(MainActivity.this,getString(R.string.diconnectA5),Toast.LENGTH_SHORT).show();
//                }else
//                {
//                    switchCamera();
//                }
                switchCamera();

            }
        });

        btn_mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleMic();
            }
        });

        MediaConstraints audioConstarints = new MediaConstraints();
        audioConstarints.mandatory.add(new MediaConstraints.KeyValuePair("googNoiseSuppression", "true"));
        audioConstarints.mandatory.add(new MediaConstraints.KeyValuePair("googEchoCancellation", "true"));

        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audioManager.setSpeakerphoneOn(true);
        audioManager.requestAudioFocus(null, 0, 2);
        audioManager.setStreamVolume(AudioManager.MODE_IN_COMMUNICATION, 100, 0);

//        PeerConnectionFactory.InitializationOptions.Builder optionBuilder =
//        PeerConnectionFactory.InitializationOptions.builder(this);
//        optionBuilder.setEnableInternalTracer(true);
//        optionBuilder.setFieldTrials("WebRTC-FlexFEC-03/Enabled/");
//        optionBuilder.setEnableVideoHwAcceleration(true);
//        PeerConnectionFactory.initialize(optionBuilder.createInitializationOptions());

        initVideos();

        PeerConnectionFactory.InitializationOptions initializationOptions =
                PeerConnectionFactory.InitializationOptions.builder(this)
                        .createInitializationOptions();
        PeerConnectionFactory.initialize(initializationOptions); // Render EGL Context

        peerConnectionFactory = new PeerConnectionFactory();
        peerConnectionFactory.setVideoHwAccelerationOptions(rootEglBase.getEglBaseContext(), rootEglBase.getEglBaseContext());

         if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
             videoCapturerAndroid =  createCameraCapturer(new Camera2Enumerator(MainActivity.this));
        } else {
            videoCapturerAndroid = createCameraCapturer(new Camera1Enumerator(false));
        }
        audioConstraints = new MediaConstraints();
        videoConstraints = new MediaConstraints();

        //Create a VideoSource instance
        if (videoCapturerAndroid != null) {
            videoSource = peerConnectionFactory.createVideoSource(videoCapturerAndroid);
        }
        localVideoTrack = peerConnectionFactory.createVideoTrack("100", videoSource);

        //create an AudioSource instance
        audioSource = peerConnectionFactory.createAudioSource(audioConstraints);
        localAudioTrack = peerConnectionFactory.createAudioTrack("101", audioSource);

        localVideoTrack.setEnabled(true);
        localAudioTrack.setEnabled(true);

        if (videoCapturerAndroid != null) {
            videoCapturerAndroid.startCapture(1024, 720, 30);
        }
        localVideoView.setVisibility(View.VISIBLE);
        // And finally, with our VideoRenderer ready, we
        // can add our renderer to the VideoTrack.
        localVideoTrack.addSink(localVideoView);

        localVideoView.setMirror(true);
        remoteVideoView.setMirror(true);


//        int audioSource1 = MediaRecorder.AudioSource.VOICE_COMMUNICATION;
//        int sampleFreq = 16000;
//        int numChannels = 1;
//        int numBytesPerSample = 2;
//        int channelConfig = numChannels == 1 ? AudioFormat.CHANNEL_IN_MONO : AudioFormat.CHANNEL_IN_STEREO;
//        int audioFormat = numBytesPerSample == 2 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT;
//        int bufSize = AudioRecord.getMinBufferSize(sampleFreq, channelConfig, audioFormat);
//        if (bufSize == AudioRecord.ERROR_BAD_VALUE || bufSize == AudioRecord.ERROR) {
//            return;
//        }
//
//        AudioRecord audioInRecord = new AudioRecord(audioSource1, sampleFreq,
//                channelConfig, audioFormat, bufSize);
//        if (audioInRecord.getState() != AudioRecord.STATE_INITIALIZED) {
//            return;
//        }
//
//        boolean aecAvailable = AcousticEchoCanceler.isAvailable();
//        if (aecAvailable) {
//            AcousticEchoCanceler instance;
//            if ((instance = AcousticEchoCanceler.create(audioInRecord.getAudioSessionId())) != null) {
//                instance.setEnabled(true);
//                Log.d("acoustic", "AEC enabled");
//            } else {
//                Log.d("acoustic", "AEC disabled");
//            }
//        }
//
//
//        AudioSource audioSource = peerConnectionFactory.createAudioSource(audioConstarints);
//        localAudioTrack = peerConnectionFactory.createAudioTrack(AUDIO_TRACK_ID, audioSource);
//        localAudioTrack.setEnabled(true);
//
//        videoCapturerAndroid = VideoCapturerAndroid.create(VideoCapturerAndroid.getNameOfFrontFacingDevice(), null);
//
//        localVideoSource = peerConnectionFactory.createVideoSource(videoCapturerAndroid, new MediaConstraints());
//        localVideoTrack = peerConnectionFactory.createVideoTrack(VIDEO_TRACK_ID, localVideoSource);
//        localVideoTrack.setEnabled(true);
//
//        localMediaStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID);
//        localMediaStream.addTrack(localVideoTrack);
//        localMediaStream.addTrack(localAudioTrack);
//
//        localVideoView = (SurfaceViewRenderer) findViewById(R.id.glview_call);
////        frameLayout = (FrameLayout) findViewById(R.id.glview_call);
//        remoteVideoView = (SurfaceViewRenderer) findViewById(R.id.glview_call1);
//
//        VideoRendererGui.setView(localVideoView, null);
//        try {
//            otherPeerRenderer = VideoRendererGui.createGui(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//            localPeerRenderer = VideoRendererGui.createGui(67, 67, 30, 30, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//            localVideoTrack.addRenderer(otherPeerRenderer);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



//        localVideoView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (connected){
//                    front = !front;
//                    newPeerRenderer.dispose();
//                    localPeerRenderer.dispose();
//                    try {
//                        newPeerRenderer = VideoRendererGui.createGui(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//                        localPeerRenderer = VideoRendererGui.createGui(67, 67, 30, 30, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//                        if (front){
//                            getLocalVideoTrack.addRenderer(newPeerRenderer);
//                            localVideoTrack.addRenderer(localPeerRenderer);
//                        }else {
//                            getLocalVideoTrack.addRenderer(localPeerRenderer);
//                            localVideoTrack.addRenderer(newPeerRenderer);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
        mediaPlayer = new MediaPlayer();
        surfaceHolder_other = gl_other_video.getHolder();
        surfaceHolder_local = gl_local_video.getHolder();
        surfaceHolder_other.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Log.d("RTCAPP", "On created");
                mediaPlayer.setDisplay(holder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });

        surfaceHolder_local.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                surfaceHolder = holder;

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                Log.e("RTCAPP", " surface changed");
//                mCamera.startPreview();

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                Log.e("RTCAPP", " surface destroyed");
                if (mCamera != null) {
                    mCamera.release();
                    mCamera = null;
                }
            }
        });
        if (Utiles.GetFirstValue("speaker")) {
            audioManager.setSpeakerphoneOn(true);
            btn_speaker.setImageResource(R.drawable.speaker_icon1);

        } else {
            audioManager.setSpeakerphoneOn(false);
            btn_speaker.setImageResource(R.mipmap.ic_earpiece);
        }

        if (Utiles.GetFirstValue("mic")) {
            localAudioTrack.setEnabled(true);
            btn_mic.setImageResource(R.mipmap.ic_mic);
        } else {
            localAudioTrack.setEnabled(false);
            btn_mic.setImageResource(R.mipmap.ic_mic_off);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onConnect();
                isrunning = false;
            }
        }, 1000);

    }

    private void initVideos() {
        rootEglBase = EglBase.create();
        localVideoView.init(rootEglBase.getEglBaseContext(), null);
        remoteVideoView.init(rootEglBase.getEglBaseContext(), null);
//        localVideoView.setZOrderMediaOverlay(true);
//        remoteVideoView.setZOrderMediaOverlay(true);
    }

//    private void ReInitControl() {
//
////        VideoRendererGui.
////        localVideoView.setVisibility(View.VISIBLE);
////        remoteVideoView.onPause();
////        remoteVideoView.setVisibility(View.GONE);
////        VideoRendererGui.(localVideoView,null);
//
//        arcProgress.setVisibility(View.VISIBLE);
//        try {
//            otherPeerRenderer = VideoRendererGui.createGui(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//            localPeerRenderer = VideoRendererGui.createGui(67, 67, 30, 30, VideoRendererGui.ScalingType.SCALE_ASPECT_FILL, true);
//            localVideoTrack.addRenderer(otherPeerRenderer);
//            localVideoTrack.setEnabled(true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        TvNext.setVisibility(View.GONE);
//        arcProgress.setVisibility(View.VISIBLE);
//        btn_callEnd.setVisibility(View.VISIBLE);
//        tv_status.setVisibility(View.VISIBLE);
//        chronometer.setVisibility(View.GONE);
//
//        chronometer.stop();
//
//        audioManager.setMode(AudioManager.MODE_NORMAL);
//        audioManager.setSpeakerphoneOn(false);
//        localVideoSource.stop();
//
//        peerConnection = null;
////        SocketConnection.connect(MainActivity.this).disconnect();
//        SocketConnection.connect(MainActivity.this).emit(Constant.Exit);
//        mediaPlayer.release();
//        if (mCamera != null) {
//            mCamera.release();
//        }
//        onConnect();
//    }

    public void switchCamera() {
        Log.d("onclick change",String.valueOf(nouser));
        if (nouser) {
            if (cameraBack) {
                mCamera.release();
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                mCamera.setDisplayOrientation(90);
                try {
                    mCamera.setPreviewDisplay(surfaceHolder);
                } catch (IOException exception) {
                    mCamera.release();
                    mCamera = null;
                }
                mCamera.startPreview();
                cameraBack = false;
            } else {
                mCamera.release();
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                mCamera.setDisplayOrientation(90);
                try {
                    mCamera.setPreviewDisplay(surfaceHolder);
                } catch (IOException exception) {
                    mCamera.release();
                    mCamera = null;
                }
                mCamera.startPreview();
                cameraBack = true;
            }


        } else {
//            front = !front;
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
//                videoCapturerAndroid =  createCameraCapturer(new Camera2Enumerator(MainActivity.this));
//            } else {
//                videoCapturerAndroid = createCameraCapturer(new Camera1Enumerator(false));
//            }
            if (videoCapturerAndroid != null) {
                if (videoCapturerAndroid instanceof CameraVideoCapturer) {
                    CameraVideoCapturer cameraVideoCapturer = (CameraVideoCapturer) videoCapturerAndroid;
                    cameraVideoCapturer.switchCamera(null);
                } else {
                    // Will not switch camera, video capturer is not a camera
                }
            }
        }

    }

    public void toggleMic() {

        if (Utiles.GetFirstValue("mic")) {
            localAudioTrack.setEnabled(false);
            btn_mic.setImageResource(R.drawable.mute_icon1);
            Utiles.SaveFirstValue("mic", false);
        } else {
            localAudioTrack.setEnabled(true);
            btn_mic.setImageResource(R.mipmap.ic_mic);
            Utiles.SaveFirstValue("mic", true);
        }

//        if (mics) {
//            localAudioTrack.setEnabled(false);
//            btn_mic.setBackgroundResource(R.mipmap.ic_mic_off);
//            mics = false;
//        } else {
//            localAudioTrack.setEnabled(true);
//            btn_mic.setBackgroundResource(R.mipmap.ic_mic);
//            mics = true;
//        }

    }

    public void toggleSpeaker() {

        if (Utiles.GetFirstValue("speaker")) {
            audioManager.setSpeakerphoneOn(false);
            btn_speaker.setImageResource(R.mipmap.ic_earpiece);
            Utiles.SaveFirstValue("speaker", false);

        } else {
            audioManager.setSpeakerphoneOn(true);
            btn_speaker.setImageResource(R.drawable.speaker_icon1);
            Utiles.SaveFirstValue("speaker", true);
        }

//        if (speaker) {
//            audioManager.setSpeakerphoneOn(true);
//            btn_speaker.setBackgroundResource(R.mipmap.ic_earpiece);
//            speaker = false;
//
//        } else {
//            audioManager.setSpeakerphoneOn(false);
//            btn_speaker.setBackgroundResource(R.mipmap.ic_speaker);
//            speaker = true;
//        }
    }

    public void disconnectCall() {
        Log.e("RTCAPP", "on disconnect" + String.valueOf(isrunning));
        if (isrunning) {

//            StartCounting();
//            Toast.makeText(MainActivity.this, getString(R.string.stopafter2), Toast.LENGTH_SHORT).show();

        } else {
//            Toast.makeText(MainActivity.this, getString(R.string.disconnect_toast), Toast.LENGTH_SHORT).show();
            if (connected) {
                Log.e("RTCAPP", "on stopped");
                onLeave();
            }
            Disconnect();
        }
    }

    private void addStreamToLocalPeer() {
        //creating local mediastream
        MediaStream stream = peerConnectionFactory.createLocalMediaStream("102");
        stream.addTrack(localAudioTrack);
        stream.addTrack(localVideoTrack);
        peerConnection.addStream(stream);
    }

    private void updateVideoViews(boolean remoteVisible) {
        runOnUiThread(() -> {
            ViewGroup.LayoutParams params = localVideoView.getLayoutParams();
            if (remoteVisible) {
                params.width = ((int) getResources().getDimension(R.dimen._120sdp));
                params.height = ((int) getResources().getDimension(R.dimen._180sdp));
                ((FrameLayout.LayoutParams) params).setMargins(0,0,(int) getResources().getDimension(R.dimen._5sdp),(int) getResources().getDimension(R.dimen._100sdp));

            } else {
                params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }
            localVideoView.setLayoutParams(params);
            localVideoView.setZOrderOnTop(true);
//            localVideoView.setZOrderMediaOverlay(true);

        });

    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void onConnect() {

        Log.e("RTCAP", "ONCONNECT");

        if (peerConnection != null)
            return;
        Log.e("peerconection", "yes");
        ArrayList<PeerConnection.IceServer> iceServers = new ArrayList<>();
        iceServers.add(new PeerConnection.IceServer("stun:stun.l.google.com:19302"));

        peerConnection = peerConnectionFactory.createPeerConnection(
                iceServers,
                new MediaConstraints(),
                peerConnectionObserver);

        addStreamToLocalPeer();
        Log.d("isconnected", String.valueOf(SocketConnection.connect(MainActivity.this).connected()));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name",getAPIKey());
            if (getIntent().getStringExtra("from")!=null){
                if (getIntent().getStringExtra("from").equals("history")){
                    if (fromUser!=null){
                        Log.d("touser",fromUser.get_id());
                        Log.d("fromuser",modelUserData.get_id());
                        Log.d("socketid",SocketConnection.connect(MainActivity.this).id());
                        jsonObject.put(Constant.FROMUSER,modelUserData.get_id());
                        jsonObject.putOpt(Constant.TOUSER,fromUser.get_id());
                        SocketConnection.connect(MainActivity.this).emit(Constant.DoConnectUser,jsonObject);
                        SocketConnection.connect(MainActivity.this).emit(Constant.Test);
                    }
                }

            }else {
                SocketConnection.connect(MainActivity.this).emit(Constant.DoConnect,jsonObject);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



//        try {
////            IO.Options options = new IO.Options();
////            encrypt = Base64.encodeToString(getAPIKey().getBytes(), Base64.DEFAULT);
////            StringBuilder stringBuilder = new StringBuilder();
////            if(modelUserData.getCoin() > Integer.valueOf(getString(R.string.minimume_coin_to_connect))){
////                if (!sessionManager.getPref(Constant.COUNTRY).equals(Constant.DEFAULT)){
////                    stringBuilder.append("&" + "country=" + sessionManager.getPref(Constant.COUNTRY));
////                }
////                if (!sessionManager.getPref(Constant.GENDER).equals(Constant.BOTH)){
////                    stringBuilder.append("&" + "gender=" + sessionManager.getPref(Constant.GENDER));
////                }
////            }
////            Log.d("auth",sessionManager.getPref(Constant.JWT));
////            options.query = "auth_token=" + sessionManager.getPref(Constant.JWT) + stringBuilder.toString() ;
////            SocketConnection.connect(MainActivity.this) = IO.SocketConnection.connect(MainActivity.this)(Links.URL, options);
//            SocketConnection.connect(MainActivity.this).connect();
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



        SocketConnection.connect(MainActivity.this)
                .on(CREATEOFFER, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    Log.e("RTCAPP", "onCreateOffer from " + ((JSONObject) args[0]).getString("id"));
                    To = ((JSONObject) args[0]).getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                createOffer = true;
                peerConnection.createOffer(sdpObserver, new MediaConstraints());
            }

        }).on(OFFER, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    Log.e("RTCAPP", "onOffer");
                    JSONObject obj = (JSONObject) args[0];
                    SessionDescription sdp = new SessionDescription(SessionDescription.Type.OFFER,
                            obj.getString(SDP));
                    To = obj.getString("from");
                    Log.e("from", To);
                    peerConnection.setRemoteDescription(sdpObserver, sdp);

                    peerConnection.createAnswer(sdpObserver, new MediaConstraints());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }).on(ANSWER, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.e("RTCAPP", "onAnswer");

                try {
                    JSONObject obj = (JSONObject) args[0];
                    SessionDescription sdp = new SessionDescription(SessionDescription.Type.ANSWER,
                            obj.getString(SDP));
                    To = obj.getString("from");
                    Log.e("from", To);

                    peerConnection.setRemoteDescription(sdpObserver, sdp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }).on(CANDIDATE, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    Log.e("RTCAPP", "onCandidate");
                    JSONObject obj = (JSONObject) args[0];
                    peerConnection.addIceCandidate(new IceCandidate(obj.getString(SDP_MID),
                            obj.getInt(SDP_M_LINE_INDEX),
                            obj.getString(SDP)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }).once(NOUSER, new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                nouser = true;
//                isrunning = true;
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(getString(R.string.app_name));
                builder.setMessage(getString(R.string.buddyLeft));
                builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        disconnect = true;
                        Disconnect();
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        disconnect = true;
                        Disconnect();
                    }
                });


                final JSONObject obj = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("RTCAPP", "on no user");
                        try {
                            final Uri video = Uri.parse(Links.VIDEO_URI + obj.getString("url"));
                            Log.e("RTCAPP", "on no user" + obj.getString("url"));

                            if (!isFinishing()) {
                                mediaPlayer.setDataSource(Links.VIDEO_URI + obj.getString("url"));
                                mediaPlayer.prepareAsync();
                                mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
                                mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                                    @Override
                                    public boolean onError(MediaPlayer mp, int what, int extra) {
                                        Log.d("RTCAPP", "On error");
                                        return false;
                                    }
                                });

                                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        Log.d("RTCAPP", "On prepare");

                                        localVideoView.setVisibility(View.GONE);
                                        tv_status.setVisibility(View.GONE);
                                        isrunning = false;
//                                        localVideoSource.stop();
//                                        localVideoTrack.removeRenderer(otherPeerRenderer);
                                        peerConnection.close();
                                        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                                        mCamera.setDisplayOrientation(90);
                                        try {
                                            mCamera.setPreviewDisplay(surfaceHolder);
                                        } catch (IOException exception) {
                                            mCamera.release();
                                            mCamera = null;
                                        }
                                        mCamera.startPreview();

                                        mediaPlayer.start();
                                        chronometer.setVisibility(View.VISIBLE);
                                        chronometer.setBase(SystemClock.elapsedRealtime());
                                        chronometer.start();
                                    }
                                });

                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        if (!isFinishing()) {
                                            builder.create().show();
                                        }
                                    }
                                });

                                mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                                    @Override
                                    public boolean onError(MediaPlayer mp, int what, int extra) {
                                        isrunning = false;
                                        if (!isFinishing()) {
                                            builder.create().show();
                                        }
                                        return true;
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }
        }).once(Leave, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chronometer.stop();
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(getString(R.string.app_name));
                        builder.setMessage(getString(R.string.buddyLeft));
                        builder.setPositiveButton(getString(R.string.countinue), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                disconnect = true;
                                Disconnect();
//                                ReInitControl();
                            }
                        });
                        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                disconnect = true;
                                Disconnect();
//                                ReInitControl();
                            }
                        });
                        if (!isFinishing()) {
                            builder.create().show();
                        }
//                        Disconnect();
//                        ReInitControl();
                    }
                });
            }
        }).once(DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Toast.makeText(MainActivity.this, getString(R.string.disconnect_toast), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).once(Constant.NOAUTH, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,getString(R.string.user_not_authenticated),Toast.LENGTH_SHORT).show();
                        Disconnect();
                    }
                });

            }
        });

        SocketConnection.connect(MainActivity.this).once(Constant.Test, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG,"Test");
            }
        });

        SocketConnection.connect(MainActivity.this).once(Constant.NoMatchingFound, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!connected) {
                            if(!isFinishing()){
                                Log.d(TAG,"NoMatchingFound");
                                SocketConnection.connect(MainActivity.this).emit(Constant.NoMatchingFoundAgain);
                            }
                        }
                    }
                }, 25000);
                Log.d(TAG, "no matching found");
//                Log.d(TAG, SocketConnection.connect(MainActivity.this).id());
//                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (!connected) {
//                            if(isFinishing()){
//                                Log.d(TAG,"NoMatchingFound");
//                                SocketConnection.connect(MainActivity.this).emit(Constant.NoMatchingFoundAgain);
//                            }
//                        }
//                    }
//                }, 15000);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (!disconnect) {
                Log.e("RTCAPP", "on stopped");
                onLeave();
//                SocketConnection.connect(MainActivity.this).disconnect();
//                SocketConnection.connect(MainActivity.this).emit(Constant.Exit);
//                Disconnect();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Disconnect() {

        chronometer.stop();

        audioManager.setMode(AudioManager.MODE_NORMAL);
        audioManager.setSpeakerphoneOn(false);
//        localVideoSource.stop();

        peerConnection.close();
//        SocketConnection.connect(MainActivity.this).disconnect();
        SocketConnection.connect(MainActivity.this).emit(Constant.Exit);
        mediaPlayer.release();
        if (mCamera != null) {
            mCamera.release();
        }
        finish();
//        intent = new Intent(MainActivity.this,SecondActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
    }

    public void onLeave() {
        Log.e("onLeave", "true");
        try {
            JSONObject obj = new JSONObject();
            obj.put("To", To);
            SocketConnection.connect(MainActivity.this).emit(Leave, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
