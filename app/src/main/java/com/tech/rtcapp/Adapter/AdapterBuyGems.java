package com.tech.rtcapp.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anjlab.android.iab.v3.SkuDetails;
import com.tech.rtcapp.Fragments.CoinFragment;
import com.tech.rtcapp.INTERFACE.InterfaceBuyGems;
import com.tech.rtcapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterBuyGems extends RecyclerView.Adapter<AdapterBuyGems.BuyGemsViewHolder> {

    List<SkuDetails> buyCoinsArrayList;
    LayoutInflater inflater;

    ArrayList<String> idList;
    InterfaceBuyGems interfaceBuyGems;
    CoinFragment coinFragment;

    public AdapterBuyGems( List<SkuDetails> coinsModelArrayList, @Nullable CoinFragment coinFragment, Context context) {
        this.buyCoinsArrayList = coinsModelArrayList;
        inflater = LayoutInflater.from(coinFragment.getActivity());
        this.coinFragment = coinFragment;
    }

    @NonNull
    @Override
    public BuyGemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BuyGemsViewHolder(inflater.inflate(R.layout.item_gemshop_grid,parent,false));

    }

    @Override
    public void onBindViewHolder(@NonNull BuyGemsViewHolder holder,final int position) {
        holder.txt_name.setText(buyCoinsArrayList.get(position).description);
        holder.btn_buy.setText(buyCoinsArrayList.get(position).priceText);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coinFragment!=null){
                    coinFragment.buyGems(buyCoinsArrayList.get(position).productId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return buyCoinsArrayList.size();
    }

    class BuyGemsViewHolder extends RecyclerView.ViewHolder{

        ImageView img_gem,img_badge;
        TextView txt_name,btn_buy;

        public BuyGemsViewHolder(View itemView) {
            super(itemView);
            img_gem = (ImageView) itemView.findViewById(R.id.gem_image);
            txt_name = (TextView) itemView.findViewById(R.id.name);
            btn_buy = (TextView)  itemView.findViewById(R.id.buyButton);
            img_badge = (ImageView) itemView.findViewById(R.id.badge_mark);
        }
    }

}
