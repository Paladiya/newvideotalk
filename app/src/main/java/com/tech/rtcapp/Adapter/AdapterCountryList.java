package com.tech.rtcapp.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.rtcapp.Constant;
import com.tech.rtcapp.Fragments.ExploreFragment;
import com.tech.rtcapp.Models.ModelUserData;
import com.tech.rtcapp.R;
import com.tech.rtcapp.SessionManager;
import com.tech.rtcapp.API.SocketConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import static com.tech.rtcapp.SecondActivity.ahBottomNavigation;

public class AdapterCountryList extends RecyclerView.Adapter<AdapterCountryList.CountryListViewHolder> {

    Context secondActivity;
    ArrayList<String> countrylist;
    public static String selected;
    public static int posi = -1;
    ModelUserData userData;
    SessionManager sessionManager;
    View vGemsBuy;
    AlertDialog.Builder dialogBuilder;
    AlertDialog dialogGemsBuy;
    TextView btn_buy_cancel,btn_buy_purchase;



    public AdapterCountryList(Context secondActivity, ArrayList<String> countryList) {
        this.secondActivity = secondActivity;
        this.countrylist = countryList;
        sessionManager = new SessionManager(secondActivity);
        userData = sessionManager.getUserDetails();
        initGemsBuyDialog();
        posi =-1;
        for (int i=0; i<countryList.size(); i++){
            if (sessionManager.getPref(Constant.COUNTRY).equals(countryList.get(i))){
                posi = i;
            }
        }
    }


    @NonNull
    @Override
    public CountryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CountryListViewHolder(LayoutInflater.from(secondActivity).inflate(R.layout.item_region_filter_dialog,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CountryListViewHolder holder, int position) {
        holder.txt_name.setText(countrylist.get(position));
        holder.img_rad_btn.setSelected(posi == position);
        holder.tvPrice.setText(String.valueOf(secondActivity.getResources().getInteger(R.integer.coins_for_location)));
//        Log.d("position",countrylist.get(position));
        if (Objects.equals(countrylist.get(position),sessionManager.getPref(Constant.COUNTRY))){
            holder.img_rad_btn.setSelected(posi == position);

        }
    }

    private void initGemsBuyDialog(){
        vGemsBuy = LayoutInflater.from(secondActivity).inflate(R.layout.dialog_rounded_corner, null);

        dialogBuilder = new AlertDialog.Builder(secondActivity);
        dialogGemsBuy = dialogBuilder.create();
        dialogGemsBuy.setView(vGemsBuy);

        btn_buy_cancel = (TextView)vGemsBuy.findViewById(R.id.left_button);
        btn_buy_purchase = (TextView)vGemsBuy.findViewById(R.id.right_button);

        btn_buy_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogGemsBuy.dismiss();
            }
        });

        btn_buy_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogGemsBuy.dismiss();
//                secondActivity.startActivity(new Intent(secondActivity, GemShopActivity.class));
                ahBottomNavigation.setCurrentItem(0);

            }
        });

    }

    @Override
    public int getItemCount() {
        return countrylist.size();
    }

    class CountryListViewHolder extends RecyclerView.ViewHolder{

        ImageView img_rad_btn;
        TextView txt_name,tvPrice;

        public CountryListViewHolder(View itemView) {
            super(itemView);
            txt_name = (TextView)itemView.findViewById(R.id.titleView);
            img_rad_btn = (ImageView)itemView.findViewById(R.id.regionRadioBtn);
            tvPrice = (TextView)itemView.findViewById(R.id.tv_price);
            View.OnClickListener l = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    posi = getAdapterPosition();
                    ExploreFragment.rd_default_region.setSelected(false);
                    notifyItemRangeChanged(0, countrylist.size());
                    if (userData.getCoin() < secondActivity.getResources().getInteger(R.integer.coins_for_location) )
                    {
                        dialogGemsBuy.show();
                    }else {
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(Constant.COUNTRY,countrylist.get(getAdapterPosition()));
                            SocketConnection.connect(secondActivity).emit(Constant.SETCOUNTRY,jsonObject);
                            sessionManager.setPref(Constant.COUNTRY,countrylist.get(getAdapterPosition()));
                            ExploreFragment.btn_location.setText(countrylist.get(getAdapterPosition()));
                        } catch (JSONException e) {
//                            Toast.makeText(SecondActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
            };
            img_rad_btn.setOnClickListener(l);
            itemView.setOnClickListener(l);
        }
    }

}
