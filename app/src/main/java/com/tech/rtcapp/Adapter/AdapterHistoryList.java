package com.tech.rtcapp.Adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tech.rtcapp.Constant;
import com.tech.rtcapp.Fragments.HistoryFragment;
import com.tech.rtcapp.INTERFACE.InterfaceShowDialog;
import com.tech.rtcapp.Models.ModelHistory;
import com.tech.rtcapp.ProfileActivity;
import com.tech.rtcapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
//import io.realm.RealmResults;

public class AdapterHistoryList extends RecyclerView.Adapter<AdapterHistoryList.MyViewHolder> {

    ArrayList<ModelHistory> historyArrayList;
    Context profileActivity;
    LayoutInflater inflater;
    InterfaceShowDialog interfaceShowDialog;


    public AdapterHistoryList(Context profileActivity, ArrayList<ModelHistory> historyArrayList, HistoryFragment historyFragment) {
        this.profileActivity = profileActivity;
        this.historyArrayList = historyArrayList;
        inflater = LayoutInflater.from(profileActivity);
        interfaceShowDialog = ((InterfaceShowDialog)historyFragment);
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(inflater.inflate(R.layout.item_hitory_adapter,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        ModelHistory modelHistory = historyArrayList.get(position);

        holder.IvImage.setImageDrawable(modelHistory.getGender().equals(Constant.MALE) ? profileActivity.getResources().getDrawable(R.drawable.img_male) : profileActivity.getResources().getDrawable(R.drawable.img_female));
        holder.TvName.setText(modelHistory.getName());
        holder.TvTime.setText(modelHistory.getCreated());
        holder.TvCountry.setText(modelHistory.getCountry());
        holder.LinRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               interfaceShowDialog.showDialog(modelHistory.getClientId());
            }
        });
    }

    private  String convertMongoDate(String val){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat= new SimpleDateFormat("HH:mm dd/MM/yyyy");
        try {
            String finalStr = outputFormat.format(inputFormat.parse(val));
            System.out.println(finalStr);
            return finalStr;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return historyArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView TvName,TvTime, TvCountry;
        CircleImageView IvImage;
        LinearLayout LinRoot;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            TvName = (TextView)itemView.findViewById(R.id.tv_name);
            TvCountry = (TextView)itemView.findViewById(R.id.tv_country);
            TvTime = (TextView)itemView.findViewById(R.id.tv_time);
            IvImage = (CircleImageView) itemView.findViewById(R.id.iv_profile);
            LinRoot = (LinearLayout)itemView.findViewById(R.id.lin_root);
        }
    }


}
