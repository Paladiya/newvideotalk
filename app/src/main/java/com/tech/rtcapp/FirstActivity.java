package com.tech.rtcapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class FirstActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    LinearLayout lin_male,lin_female;
    Utiles utiles;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_first);

        utiles = new Utiles(this);

        if (!utiles.GetFirstValue("IsFirst"))
        {
            finish();
            startActivity(new Intent(FirstActivity.this,SecondActivity.class));

        }


        findViewById(R.id.gender_malebtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utiles.SaveFirstValue("IsFirst",false);
                finish();
                startActivity(new Intent(FirstActivity.this,SecondActivity.class));
            }
        });

        findViewById(R.id.gender_femalebtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utiles.SaveFirstValue("IsFirst",false);
                finish();
                startActivity(new Intent(FirstActivity.this,SecondActivity.class));
            }
        });

        progressDialog = Utiles.getProgressDialog("Please Wait",this);

    }
}
