package com.tech.rtcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tech.rtcapp.API.SocketConnection;
import com.tech.rtcapp.Models.ModelUserData;


public class CallActivity extends AppCompatActivity {

    ImageView ivCallAccept, ivCallReject;
    TextView tvCallerName, tvCountryName, tvAppName;
    Vibrator vibrator;
    MediaPlayer mediaPlayer;
    ModelUserData modelUserData;
    Intent intent;
    private GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        Window oWindow = this.getWindow();



        modelUserData = new Gson().fromJson(getIntent().getStringExtra("user"),ModelUserData.class);

        oWindow.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                + WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Android_Gesture_Detector  android_gesture_detector  =  new Android_Gesture_Detector();
// Create a GestureDetector
        mGestureDetector = new GestureDetector(this, android_gesture_detector);

        SocketConnection.connect(CallActivity.this);

        mediaPlayer = MediaPlayer.create(this, R.raw.ringtone);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        ivCallAccept = (ImageView)findViewById(R.id.imageViewcallAccept);
        ivCallReject = (ImageView)findViewById(R.id.imageViewCallReject);

        tvCallerName = (TextView)findViewById(R.id.txt_callername);
        tvCountryName = (TextView)findViewById(R.id.txt_country);
        tvAppName = (TextView)findViewById(R.id.txt_app_name);
        String title = getString(R.string.voicecall,getString(R.string.app_name));
        tvAppName.setText(title);

        long[] mVibratePattern = new long[]{0,400,400};

        vibrator.vibrate(mVibratePattern,0);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        tvCallerName.setText(modelUserData.getName());
        tvCountryName.setText(modelUserData.getCountry());

        intent = new Intent(this,MainActivity.class);
        intent.putExtra("from","history");
        intent.putExtra("user",new Gson().toJson(modelUserData));
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        ivCallAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopMedia();
                finish();
                startActivity(intent);
            }
        });

        ivCallReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               stopMedia();
               finishAffinity();
            }
        });

    }

    private void stopMedia() {
        mediaPlayer.stop();
        vibrator.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopMedia();
    }


    class Android_Gesture_Detector implements GestureDetector.OnGestureListener,
            GestureDetector.OnDoubleTapListener {

        @Override
        public boolean onDown(MotionEvent e) {
            Log.d("Gesture ", " onDown");
            return true;
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.d("Gesture ", " onSingleTapConfirmed");
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Log.d("Gesture ", " onSingleTapUp");
            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            Log.d("Gesture ", " onShowPress");
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.d("Gesture ", " onDoubleTap");
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            Log.d("Gesture ", " onDoubleTapEvent");
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            Log.d("Gesture ", " onLongPress");
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            Log.d("Gesture ", " onScroll");
            if (e1.getY() < e2.getY()){
                Log.d("Gesture ", " Scroll Down");
            }
            if(e1.getY() > e2.getY()){
                Log.d("Gesture ", " Scroll Up");
            }
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX() < e2.getX()) {
                Log.d("Gesture ", "Left to Right swipe: "+ e1.getX() + " - " + e2.getX());
                Log.d("Speed ", String.valueOf(velocityX) + " pixels/second");
            }
            if (e1.getX() > e2.getX()) {
                Log.d("Gesture ", "Right to Left swipe: "+ e1.getX() + " - " + e2.getX());
                Log.d("Speed ", String.valueOf(velocityX) + " pixels/second");
            }
            if (e1.getY() < e2.getY()) {
                Log.d("Gesture ", "Up to Down swipe: " + e1.getX() + " - " + e2.getX());
                Log.d("Speed ", String.valueOf(velocityY) + " pixels/second");
            }
            if (e1.getY() > e2.getY()) {
                Log.d("Gesture ", "Down to Up swipe: " + e1.getX() + " - " + e2.getX());
                Log.d("Speed ", String.valueOf(velocityY) + " pixels/second");
            }
            return true;

        }


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);

    }
}
