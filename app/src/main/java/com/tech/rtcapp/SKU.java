package com.tech.rtcapp;

public class SKU {

    String sku_name,sku_time_start,getSku_time_end;

    public SKU() {
    }

    public SKU(String sku_name, String sku_time_start, String getSku_time_end) {
        this.sku_name = sku_name;
        this.sku_time_start = sku_time_start;
        this.getSku_time_end = getSku_time_end;
    }

    public String getSku_name() {
        return sku_name;
    }

    public void setSku_name(String sku_name) {
        this.sku_name = sku_name;
    }

    public String getSku_time_start() {
        return sku_time_start;
    }

    public void setSku_time_start(String sku_time_start) {
        this.sku_time_start = sku_time_start;
    }

    public String getGetSku_time_end() {
        return getSku_time_end;
    }

    public void setGetSku_time_end(String getSku_time_end) {
        this.getSku_time_end = getSku_time_end;
    }
}
