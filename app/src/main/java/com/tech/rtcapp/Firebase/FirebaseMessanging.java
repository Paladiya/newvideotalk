package com.tech.rtcapp.Firebase;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.tech.rtcapp.CallActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class FirebaseMessanging extends FirebaseMessagingService {

    Intent intent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);

        intent = new Intent(this, CallActivity.class);
        try {
            Log.d("firebase","message received "+object.getString("user"));
            intent.putExtra("user",object.getString("user"));
            intent.putExtra("from","history");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(intent);
    }


}
