package com.tech.rtcapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.Adapter.AdapterHistoryList;
import com.tech.rtcapp.INTERFACE.InterfaceShowDialog;
import com.tech.rtcapp.MainActivity;
import com.tech.rtcapp.Models.ModelHistory;
import com.tech.rtcapp.Models.ModelUserData;
import com.tech.rtcapp.R;
import com.tech.rtcapp.SecondActivity;
import com.tech.rtcapp.SessionManager;
import com.tech.rtcapp.Utiles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

//import io.realm.Realm;
//import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment implements InterfaceShowDialog {

    View view;
    APIInterface apiInterface;
    ArrayList<ModelHistory> historyArrayList;
    AdapterHistoryList adapterHistoryList;
    RecyclerView RvHistory;
    TextView TvEmpty;
    ModelUserData modelUserData;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    View purchaseView;
    TextView DiaTvPurchase;
    Intent intent;
    SessionManager sessionManager;
//    Realm realm;
    Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);

//        realm = Realm.getDefaultInstance();

        if (mContext!=null && isAdded()){
            Utiles.ShowProgres(getActivity(), getString(R.string.loading));
            sessionManager = new SessionManager(getActivity());
            apiInterface = APIClient.getClient().create(APIInterface.class);
            modelUserData = sessionManager.getUserDetails();

            RvHistory = (RecyclerView) view.findViewById(R.id.rv_history);
            TvEmpty = (TextView) view.findViewById(R.id.tv_empty);

            dialogBuilder = new AlertDialog.Builder(getActivity());
            purchaseView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_purchase, null);
            alertDialog = dialogBuilder.create();
            DiaTvPurchase = (TextView) purchaseView.findViewById(R.id.tv_purchase);
            DiaTvPurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SecondActivity.ahBottomNavigation.setCurrentItem(3);
                }
            });
            alertDialog.setView(purchaseView);
            initHistory();

        }

        return view;
    }

    private synchronized void initHistory() {

        apiInterface.GetHistoryList(modelUserData.get_id()).enqueue(new Callback<ArrayList<ModelHistory>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelHistory>> call, Response<ArrayList<ModelHistory>> response) {
                Utiles.DismissProgress();
                if (response.code() == 200) {
                    historyArrayList = response.body();
                    if (historyArrayList.size() > 0) {
                        showHistory();
                        RvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
//                            Collections.reverse(historyArrayList);
                        if (mContext!=null){
                            adapterHistoryList = new AdapterHistoryList(mContext, historyArrayList, HistoryFragment.this);
                            RvHistory.setAdapter(adapterHistoryList);
                        }

                    } else {
                        hideHistory();
                    }
                } else {
//                        Toast.makeText(getActivity(),((SecondActivity)getActivity()).getString(R.string.no_history_found),Toast.LENGTH_SHORT).show();
                    SecondActivity.ahBottomNavigation.setCurrentItem(0);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelHistory>> call, Throwable t) {
                Utiles.DismissProgress();
                Log.d("History",t.getMessage());
                Toast.makeText(getActivity(), getString(R.string.server_maintanance), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hideHistory() {
        RvHistory.setVisibility(View.GONE);
        TvEmpty.setVisibility(View.VISIBLE);
    }

    private void showHistory() {
        RvHistory.setVisibility(View.VISIBLE);
        TvEmpty.setVisibility(View.GONE);
    }


    @Override
    public void showDialog(String id) {
        if (modelUserData.getCoin() < getResources().getInteger(R.integer.call_coin))
        {
            alertDialog.show();

        }else {
            Utiles.ShowProgres(getActivity(),getString(R.string.connecting));
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Utiles.DismissProgress();
//                    Toast.makeText(getActivity(),getString(R.string.user_is_not_available_now),Toast.LENGTH_SHORT).show();
//                }
//            },3000);

            apiInterface.CallRequest(id,modelUserData.get_id()).enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Utiles.DismissProgress();
                    Log.d("respose code",String.valueOf(String.valueOf(response.code())));
                    if (response.code() == 200){
                        intent = new Intent(getActivity(), MainActivity.class);
                        intent.putExtra("from","history");
                        startActivity(intent);
                    }else {
                        Toast.makeText(getActivity(),getString(R.string.user_is_not_available_now),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Utiles.DismissProgress();
                    Log.d("respose code",String.valueOf(t.getMessage()));
                    Toast.makeText(getActivity(),getString(R.string.user_is_not_available_now),Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

}
