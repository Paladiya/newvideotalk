package com.tech.rtcapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.gson.Gson;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.Adapter.AdapterBuyGems;
import com.tech.rtcapp.INTERFACE.InterfaceBuyGems;
import com.tech.rtcapp.Models.ModelBuyCoins;
import com.tech.rtcapp.Models.ModelPurchase;
import com.tech.rtcapp.Models.ModelUserData;
import com.tech.rtcapp.R;
import com.tech.rtcapp.SecondActivity;
import com.tech.rtcapp.SessionManager;
import com.tech.rtcapp.Utiles;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoinFragment extends Fragment implements InterfaceBuyGems, BillingProcessor.IBillingHandler {

    View view;
    ImageView img_close;
    RecyclerView recyclerView;
    AdapterBuyGems adapterBuyGems;
    APIInterface apiInterface;
    public ArrayList<ModelBuyCoins> coinsModelArrayList;
    String TAG;
    ArrayList<String> idList;
    ModelPurchase purchaseModel;
    public ModelUserData userModel;
    String gemId, buyId;
    TextView tvMyGem;
    List<SkuDetails> skuDetails;
    public BillingProcessor billingProcessor;
    SessionManager sessionManager;

    Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_coin, container, false);

        sessionManager = new SessionManager(getActivity());
        userModel = sessionManager.getUserDetails();
        Utiles.ShowProgres(getActivity(), getString(R.string.plswaite));

        TAG = getClass().getSimpleName();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        billingProcessor = new BillingProcessor(getActivity(), getString(R.string.billingProcessorKey), this);
        billingProcessor.initialize();

        tvMyGem = (TextView) view.findViewById(R.id.myGem);
        tvMyGem.setText(String.valueOf(userModel.getCoin()));
        recyclerView = (RecyclerView) view.findViewById(R.id.itemList);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        img_close = (ImageView) view.findViewById(R.id.gemshopClose);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SecondActivity) getActivity()).ahBottomNavigation.setCurrentItem(0);
            }
        });

        apiInterface.GetGemsList().enqueue(new Callback<ArrayList<ModelBuyCoins>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelBuyCoins>> call, Response<ArrayList<ModelBuyCoins>> response) {
                Log.d("onresponse", "gems");
                Utiles.DismissProgress();
                if (response.code() == 200) {
                    coinsModelArrayList = response.body();
                    Log.d(TAG, new Gson().toJson(coinsModelArrayList));
                    idList = new ArrayList<>();
                    for (int i = 0; i < coinsModelArrayList.size(); i++) {
                        idList.add(coinsModelArrayList.get(i).getSubId());
                    }
                    skuDetails = billingProcessor.getPurchaseListingDetails(idList);
                    if (mContext!=null){
                        adapterBuyGems = new AdapterBuyGems(skuDetails, CoinFragment.this,mContext);
                        recyclerView.setAdapter(adapterBuyGems);
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelBuyCoins>> call, Throwable t) {
                Utiles.DismissProgress();
                Toast.makeText(getActivity(), getString(R.string.server_maintanance), Toast.LENGTH_LONG).show();
                ((SecondActivity) getActivity()).ahBottomNavigation.setCurrentItem(0);

            }
        });


        return view;
    }


    @Override
    public void buyGems(final String id) {
        Log.d("buyid", id);
        for (int i = 0; i < coinsModelArrayList.size(); i++) {
            if (coinsModelArrayList.get(i).getSubId().equals(id)) {
//Do whatever you want here
                gemId = coinsModelArrayList.get(i).get_id();
            }
        }
        buyId = id;
        billingProcessor.purchase(getActivity(), buyId);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onbilling", "onActivityResult fragment");
//        billingProcessor.consumePurchase(buyId);
        if (!billingProcessor.handleActivityResult(requestCode, resultCode, data)) {
            Log.d("onactivityresult", "called");
        }
    }

    public synchronized void RegisterPackage() {
        apiInterface.RegisterPackage(purchaseModel).enqueue(new Callback<ModelUserData>() {
            @Override
            public void onResponse(Call<ModelUserData> call, Response<ModelUserData> response) {
                Utiles.DismissProgress();
                if (response.code() == 200) {
                    userModel = response.body();
                    Log.d("user", new Gson().toJson(userModel));
                    sessionManager.updateUser(userModel);
                    tvMyGem.setText(String.valueOf(userModel.getCoin()));
                    if(isAdded())
                    Toast.makeText(getActivity(), getString(R.string.purchase_done_suc), Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(getActivity(), getString(R.string.purchase_not_done_suc), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ModelUserData> call, Throwable t) {
                Log.d("on failure", t.getMessage());
                Utiles.DismissProgress();
                Toast.makeText(getActivity(), getString(R.string.server_maintanance), Toast.LENGTH_LONG).show();

            }
        });

    }


    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        Log.d("onbilling", "purchased " + details.purchaseInfo);
        if (billingProcessor.consumePurchase(productId)) {
            for (int i = 0; i < coinsModelArrayList.size(); i++) {
                if (coinsModelArrayList.get(i).getSubId().equals(productId)) {
//Do whatever you want here
                    gemId = coinsModelArrayList.get(i).get_id();
                }
            }
            purchaseModel = new ModelPurchase();
            purchaseModel.setToken(details.purchaseToken);
            purchaseModel.setSignature(details.purchaseInfo.signature);
            purchaseModel.setOrderId(details.orderId);
            purchaseModel.setGemsId(gemId);
            purchaseModel.setUserId(userModel.get_id());
            purchaseModel.setPackageName(details.purchaseInfo.purchaseData.packageName);
            RegisterPackage();
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.d("onbilling", "onPurchaseHistoryRestored");

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        Log.d("onbilling", "error called ");

    }

    @Override
    public void onBillingInitialized() {
        Log.d("onbilling", "initialized");
    }
}
