package com.tech.rtcapp.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.API.SocketConnection;
import com.tech.rtcapp.Adapter.AdapterCountryList;
import com.tech.rtcapp.Constant;
import com.tech.rtcapp.MainActivity;
import com.tech.rtcapp.Models.ModelUserData;
import com.tech.rtcapp.R;
import com.tech.rtcapp.SecondActivity;
import com.tech.rtcapp.ShowCamera;
import com.tech.rtcapp.SignUpActivity;
import com.tech.rtcapp.Utiles;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Ack;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.content.Context.LOCATION_SERVICE;


public class ExploreFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ExploreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExploreFragment newInstance(String param1, String param2) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    View view;
    private static final int PERMISSION_REQUEST_CODE = 200;
    public static Boolean isRunning = false;
    Toolbar toolbar;
    Button btn_start, btn_cancel;
    LinearLayout lin_mainloadingview;
    Handler handler;
    public static CountDownTimer countDownTimer;
    TextView tv_match;
    String PURCHASE_ID;
    Utiles utiles;
    AlertDialog alertDialog;
    Button btn_gender, btn_coin;
    public static Button btn_location;
    ModelUserData modelUserData;
    ImageView img_male, img_female, img_both, img_gender;
    View vGender, vRegion, vGemsBuy;
    LinearLayout lin_male, lin_female, lin_both;
    TextView btn_posi_gender, btn_posi_region, btn_buy_cancel, btn_buy_purchase;
    String Gender = "BOTH";
    LayoutInflater myInflater;
    RecyclerView recyclerViewRegion;
    AlertDialog.Builder dialogBuilder;
    AlertDialog dialogGender, dialogRegion, dialogGemsBuy;
    LocationManager mLocationManager;
    Location myLocation;
    String country = " ", TAG;
    APIInterface apiInterface;
    ArrayList<String> countryList;
    FrameLayout preview;
    ShowCamera showCamera;
    Camera camera;
    public static ImageView rd_default_region;
    RelativeLayout rel_default_region;
    AdapterCountryList adapterCountryList;
    boolean isSocketConnected = false;
    CircleImageView IvProfile;
    TextView TvUserName, TvCountryName;
    RelativeLayout RvProfile;
    boolean onCreateCall = false;
    ViewGroup thisContainer;
    TextView tv_male_price, tv_female_price;
    AVLoadingIndicatorView avLoadingIndicatorView;
    Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        Utiles.ShowProgres(getActivity(),getString(R.string.loading));

        Log.d("socketcall",SocketConnection.class.getSimpleName());
        modelUserData = ((SecondActivity) getActivity()).sessionManager.getUserDetails();
        thisContainer = container;
        myInflater = LayoutInflater.from(getActivity());
        view = inflater.inflate(R.layout.fragment_explore, container, false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        
        
        if (!checkPermission()) {
            requestPermission();
        }
        TAG = getClass().getSimpleName();
        mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setPositiveButton("ACCEPT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utiles.SaveDialogShow("accept", true);
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.setIconAttribute(android.R.attr.alertDialogIcon);
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setMessage("Be polite and respectful. Sexual, smoking, violent content or nudity display behaviours are strictly prohibited, otherwise your account will be banned.");

        btn_start = (Button) view.findViewById(R.id.btn_start);
        btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
        lin_mainloadingview = (LinearLayout) view.findViewById(R.id.mainLoadingLayout);
        tv_match = (TextView) view.findViewById(R.id.tv_match);
        handler = new Handler();
        avLoadingIndicatorView = (AVLoadingIndicatorView)view.findViewById(R.id.av_progress);

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utiles.GetDialogShow("accept")) {
                    if (!checkPermission()) {
                        Log.e("onclick", "check permisssion");
                        requestPermission();
                    } else {
                        if (Utiles.isInternetOn(getActivity())) {
                            Open();

                        } else {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setIconAttribute(android.R.attr.alertDialogIcon);
                            builder.setTitle(R.string.nointernate);
                            builder.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Utiles.isInternetOn(getActivity())) {
                                        Open();
                                    } else {
                                        builder.create().show();
                                    }
                                }
                            });
                            builder.create().show();
                        }
                    }
                } else {
                    alertDialog.show();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onclick", "cancel");
                stopCalling();
            }
        });


        TvCountryName = (TextView) view.findViewById(R.id.tv_country);
        TvUserName = (TextView) view.findViewById(R.id.tv_username);
        IvProfile = (CircleImageView) view.findViewById(R.id.iv_profile);
        RvProfile = (RelativeLayout) view.findViewById(R.id.rel_profile);
        RvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SecondActivity) getActivity()).ahBottomNavigation.setCurrentItem(2);
//                startActivity(new Intent(getActivity(), ProfileActivity.class));
            }
        });

        img_gender = (ImageView)view.findViewById(R.id.iv_gender);
        if (modelUserData.getGender().equals(Constant.MALE))
        {
            img_gender.setImageDrawable(getResources().getDrawable(R.drawable.ic_select_male));
        }else {
            img_gender.setImageDrawable(getResources().getDrawable(R.drawable.ic_select_female));
        }


        preview = (FrameLayout) view.findViewById(R.id.fragment_container);
        btn_gender = (Button) view.findViewById(R.id.genderButtonTextView);
        btn_location = (Button) view.findViewById(R.id.regionButtonTextView);
        btn_coin = (Button) view.findViewById(R.id.buttonGemOnMatch);

        btn_coin.setOnClickListener(this);
        btn_gender.setOnClickListener(this);
        btn_location.setOnClickListener(this);


        fillData();
        fillData();
        initGenderDialog();
        initGemsBuyDialog();
        initLocationDialog();

        apiInterface.GetCountryList().enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.code() == 200) {
                    JsonArray jsonArray = new JsonArray();

                    countryList = new ArrayList<String>((Collection<? extends String>) response.body());
                    for (int i = 0; i < jsonArray.size(); i++) {
                        if(!jsonArray.get(i).getAsString().trim().equals("")){
                            countryList.add(jsonArray.get(i).getAsString());
                        }
                    }
                    if(isAdded()){
                        FillCountryData();
                    }

                } else {
//                    Toast.makeText(getActivity(), getString(R.string.somethingwentwrong), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.server_maintanance), Toast.LENGTH_LONG).show();
            }
        });
        Log.d("on user ","test");

        apiInterface.GetUser(modelUserData.get_id()).enqueue(new Callback<ModelUserData>() {
            @Override
            public void onResponse(Call<ModelUserData> call, Response<ModelUserData> response) {
                Utiles.DismissProgress();
                if (response.code() == 200 && response.body() != null && (!response.body().get_id().equals(" "))) {
                    Log.d(TAG, "update user " + new Gson().toJson(response.body()));

                    ModelUserData newUser = response.body();
                    Log.d("myid",modelUserData.get_id());
                    Log.d("mynewid",newUser.get_id());

                    modelUserData.setFcmToken(newUser.getFcmToken());
                    modelUserData.setCoin(newUser.getCoin());
                    modelUserData.set_id(newUser.get_id());
                    modelUserData.setCountry(newUser.getCountry()!=null?newUser.getCountry():" ");
                    modelUserData.setEmail(newUser.getEmail());
                    modelUserData.setName(newUser.getName());
                    modelUserData.setSignupType(newUser.getSignupType());
                    modelUserData.setBirthDate(newUser.getBirthDate());
                    modelUserData.setGender(newUser.getGender());
                    btn_coin.setText(String.valueOf(modelUserData.getCoin()));


                    if (isAdded()){
                        ((SecondActivity) getActivity()).sessionManager.updateUser(modelUserData);
                    }
                    if (modelUserData.getFcmToken()==null){
                        ((SecondActivity)getActivity()).updateFcmToken();
                    }

                } else {
                    Log.d("on user ","on respose");
                    Toast.makeText(getActivity(), getString(R.string.user_not_authenticated), Toast.LENGTH_LONG).show();
                    ((SecondActivity) getActivity()).sessionManager.clearAll();
                    intent = new Intent(getActivity(), SignUpActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ModelUserData> call, Throwable t) {
                Utiles.DismissProgress();
                Log.d("on user ",t.getMessage());

//                Toast.makeText(getActivity(),getString(R.string.server_maintanance),Toast.LENGTH_LONG).show();
//                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        modelUserData = ((SecondActivity) getActivity()).sessionManager.getUserDetails();

        btn_coin.setText(String.valueOf(((SecondActivity) getActivity()).sessionManager.getUserDetails().getCoin()));
        if (isRunning) {
            if (needCoin()){
                if (modelUserData.getCoin()>=getResources().getInteger(R.integer.required_coin)){
                    startCalling();
                }else {
                    stopCalling();
                    Toast.makeText(getActivity(),getString(R.string.not_enough_coin_for_matching),Toast.LENGTH_SHORT).show();
                    ((SecondActivity)getActivity()).sessionManager.setPref(Constant.GENDER,Constant.BOTH);
                    ((SecondActivity)getActivity()).sessionManager.setPref(Constant.COUNTRY,Constant.GLOBAL);
                }
            }else{
                startCalling();
            }
        }
        camera = Camera.open(1);
        showCamera = new ShowCamera(getActivity(), camera);
//        preview.removeAllViews();
        preview.addView(showCamera);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void fillData() {
//        if (!isSocketConnected) {
//            Utiles.ShowProgres(getActivity(), getString(R.string.plswaite));
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (isSocketConnected) {
//                        Utiles.DismissProgress();
//                    } else {
//                        SocketConnection.connect(getActivity()) = SocketConnection.connect(getActivity());
//                    }
//                }
//            }, 4000);
//        }
        if (modelUserData.getImage() != null) {
            try {
                IvProfile.setImageURI(Uri.parse(modelUserData.getImage()));

            } catch (Exception e) {
                IvProfile.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));

            }
        } else {
            IvProfile.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));
        }
        btn_gender.setText(((SecondActivity) getActivity()).sessionManager.getPref(Constant.GENDER));
        btn_location.setText(((SecondActivity) getActivity()).sessionManager.getPref(Constant.COUNTRY));
        if (modelUserData.getCountry()!=null){
            TvCountryName.setText(modelUserData.getCountry());
        }else {
            TvCountryName.setVisibility(View.GONE);
        }
        TvUserName.setText(modelUserData.getName());
    }


    private void initLocationDialog() {

        vRegion = myInflater.inflate(R.layout.dialog_region_filter, null);
        FillCountryData();

        btn_posi_region = ((TextView) vRegion.findViewById(R.id.positiveRegionBtn));
        btn_posi_region.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRegion.dismiss();

            }
        });

        rd_default_region = vRegion.findViewById(R.id.regionRadioBtn);
        if (((SecondActivity) getActivity()).sessionManager.getPref(Constant.COUNTRY).equals(Constant.GLOBAL)) {
            rd_default_region.setSelected(true);
        }

        rel_default_region = vRegion.findViewById(R.id.rel_default_region);
        rel_default_region.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rd_default_region.setSelected(true);
                if (countryList != null && countryList.size() > 0) {
                    AdapterCountryList.posi = -1;
                    adapterCountryList.notifyItemRangeChanged(0, countryList.size());
                }
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Constant.COUNTRY, Constant.GLOBAL);
                    SocketConnection.connect(getActivity()).emit(Constant.SETCOUNTRY, jsonObject);
                    ((SecondActivity) getActivity()).sessionManager.setPref(Constant.COUNTRY, Constant.GLOBAL);
                    btn_location.setText(((SecondActivity) getActivity()).sessionManager.getPref(Constant.COUNTRY));
                } catch (JSONException e) {
//                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogRegion = dialogBuilder.create();
        dialogRegion.setView(vRegion);

    }

    private void FillCountryData() {
        if (countryList != null && countryList.size() > 0) {
            Log.d("countrylist", String.valueOf(countryList.size()));
            adapterCountryList = new AdapterCountryList(getActivity(), countryList);
            recyclerViewRegion = ((RecyclerView) vRegion.findViewById(R.id.recyclerViewRegion));
            recyclerViewRegion.setLayoutManager(new LinearLayoutManager(getActivity()));
            if (countryList.size()>0){
                recyclerViewRegion.setAdapter(adapterCountryList);
            }
        }
    }

    private void initGemsBuyDialog() {
        vGemsBuy = myInflater.inflate(R.layout.dialog_rounded_corner, null);

        dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogGemsBuy = dialogBuilder.create();
        dialogGemsBuy.setView(vGemsBuy);

        btn_buy_cancel = (TextView) vGemsBuy.findViewById(R.id.left_button);
        btn_buy_purchase = (TextView) vGemsBuy.findViewById(R.id.right_button);

        btn_buy_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogGemsBuy.dismiss();
            }
        });

        btn_buy_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogGemsBuy.dismiss();
//                startActivity(new Intent(getActivity(), GemShopActivity.class));
                ((SecondActivity) getActivity()).ahBottomNavigation.setCurrentItem(3);
            }
        });

    }


    private void initGenderDialog() {

        vGender = myInflater.inflate(R.layout.dialog_gender_filter, null);

        btn_posi_gender = ((TextView) vGender.findViewById(R.id.positiveGenderBtn));
        lin_male = ((LinearLayout) vGender.findViewById(R.id.lin_male));
        lin_female = ((LinearLayout) vGender.findViewById(R.id.lin_female));
        lin_both = ((LinearLayout) vGender.findViewById(R.id.lin_both));

        img_male = ((ImageView) lin_male.findViewById(R.id.genderMaleRadioBtn));
        img_female = ((ImageView) lin_female.findViewById(R.id.genderFemaleRadioBtn));
        img_both = ((ImageView) lin_both.findViewById(R.id.genderBothRadioBtn));
        tv_male_price = (TextView) lin_male.findViewById(R.id.tv_price);
        tv_female_price = (TextView) lin_female.findViewById(R.id.tv_price);

        tv_male_price.setText(String.valueOf(getResources().getInteger(R.integer.coins_for_male)));
        tv_female_price.setText(String.valueOf(getResources().getInteger(R.integer.coins_for_male)));

        if (((SecondActivity) getActivity()).sessionManager.getPref(Constant.GENDER).equals(Constant.MALE)) {
            img_male.setSelected(true);
        } else if (((SecondActivity) getActivity()).sessionManager.getPref(Constant.GENDER).equals(Constant.FEMALE)) {
            img_female.setSelected(true);
        } else {
            img_both.setSelected(true);

        }

        lin_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gender = Constant.MALE;
                img_male.setSelected(true);
                img_female.setSelected(false);
                img_both.setSelected(false);
                if (modelUserData.getCoin() < getResources().getInteger(R.integer.coins_for_male)) {
                    if (dialogGender.isShowing()) {
                        dialogGender.dismiss();
                    }
                    dialogGemsBuy.show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Constant.GENDER, Constant.MALE);
                        SocketConnection.connect(getActivity()).emit(Constant.SETGENDER, jsonObject);
                        ((SecondActivity) getActivity()).sessionManager.setPref(Constant.GENDER, Constant.MALE);
                        btn_gender.setText(((SecondActivity) getActivity()).sessionManager.getPref(Constant.GENDER));
                    } catch (JSONException e) {
//                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

            }
        });

        lin_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gender = Constant.FEMALE;
                img_female.setSelected(true);
                img_male.setSelected(false);
                img_both.setSelected(false);
                if (modelUserData.getCoin() < getResources().getInteger(R.integer.coins_for_male)) {
                    if (dialogGender.isShowing()) {
                        dialogGender.dismiss();
                    }
                    dialogGemsBuy.show();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Constant.GENDER, Constant.FEMALE);
                        SocketConnection.connect(getActivity()).emit(Constant.SETGENDER, jsonObject, new Ack() {
                            @Override
                            public void call(Object... args) {
                                Log.d("call","inside");
                            }
                        });
                        ((SecondActivity) getActivity()).sessionManager.setPref(Constant.GENDER, Constant.FEMALE);
                        btn_gender.setText(((SecondActivity) getActivity()).sessionManager.getPref(Constant.GENDER));
                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }
        });

        lin_both.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gender = Constant.BOTH;
                img_both.setSelected(true);
                img_female.setSelected(false);
                img_male.setSelected(false);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Constant.GENDER, Constant.BOTH);
                    SocketConnection.connect(getActivity()).emit(Constant.SETGENDER, jsonObject);
                    ((SecondActivity) getActivity()).sessionManager.setPref(Constant.GENDER, Constant.BOTH);
                    btn_gender.setText(((SecondActivity) getActivity()).sessionManager.getPref(Constant.GENDER));
                } catch (JSONException e) {
//                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        btn_posi_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogGender.dismiss();
            }
        });
        dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogGender = dialogBuilder.create();
        dialogGender.setView(vGender);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), RECORD_AUDIO);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(), new String[]{RECORD_AUDIO, CAMERA}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean audioAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                break;

        }

    }

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void Open() {
        countDownTimer = new CountDownTimer(4000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                tv_match.setText(getString(R.string.matching) + " " + String.valueOf((millisUntilFinished / 1000)));
            }

            @Override
            public void onFinish() {
                if (SocketConnection.socket.connected()){
                    startActivity(new Intent(getActivity(), MainActivity.class));
                }else {
                    SocketConnection.connect(getActivity());
                    startActivity(new Intent(getActivity(), MainActivity.class));
                }
            }
        };
        if (needCoin()){
            if (modelUserData.getCoin()>=getResources().getInteger(R.integer.required_coin)){
                startCalling();
            }else {

                Toast.makeText(getActivity(),getString(R.string.not_enough_coin_for_matching),Toast.LENGTH_SHORT).show();
                ((SecondActivity)getActivity()).sessionManager.setPref(Constant.GENDER,Constant.BOTH);
                ((SecondActivity)getActivity()).sessionManager.setPref(Constant.COUNTRY,Constant.GLOBAL);
            }
        }else{
            startCalling();

        }

    }



    public boolean needCoin(){
        if (((SecondActivity)getActivity()).sessionManager.getPref(Constant.GENDER).equals(Constant.BOTH) &&
                ((SecondActivity)getActivity()).sessionManager.getPref(Constant.COUNTRY).equals(Constant.GLOBAL)){
            return false;
        }else {
            return true;
        }
    }

    public void startCalling(){
        isRunning = true;
        avLoadingIndicatorView.setVisibility(View.VISIBLE);
        lin_mainloadingview.setVisibility(View.VISIBLE);
        btn_start.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.VISIBLE);
        countDownTimer.start();
    }

    public void stopCalling() {
        isRunning = false;
        countDownTimer.cancel();
        btn_cancel.setVisibility(View.GONE);
        btn_start.setVisibility(View.VISIBLE);
        lin_mainloadingview.setVisibility(View.GONE);
        avLoadingIndicatorView.setVisibility(View.GONE);

    }


    public void onClickShareLink(View view) {
        final String appPackageName = getActivity().getPackageName();
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", "Click on link to connect with people around you https://play.google.com/store/apps/details?id=" + appPackageName);
        intent.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.app_name));
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Share with"));

    }


    public void showPremiumPlan(View view) {
        int titleResId;
        titleResId = R.string.subscription_period_prompt;
        final CharSequence[] options;
        options = new CharSequence[3];
        options[0] = getString(R.string.subscription_period_1day);
        options[1] = getString(R.string.subscription_period_7day);
        options[2] = getString(R.string.subscription_period_30day);
        PURCHASE_ID = getString(R.string.subscription_period_1day_sku);

        dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle(titleResId)
                .setSingleChoiceItems(options, 0 /* checkedItem */, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            PURCHASE_ID = getString(R.string.subscription_period_1day_sku);

                        } else if (which == 1) {
                            PURCHASE_ID = getString(R.string.subscription_period_7day_sku);

                        } else if (which == 2) {
                            PURCHASE_ID = getString(R.string.subscription_period_30day_sku);
                        }
                    }
                })
                .setPositiveButton(R.string.subscription_prompt_continue, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton(R.string.subscription_prompt_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        dialogBuilder.setIcon(R.mipmap.ic_launcher);
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();

    }

    @Override
    public void onClick(View v) {
//        initGenderDialog();
        switch (v.getId()) {
            case R.id.genderButtonTextView:

                dialogGender.show();

                break;
            case R.id.regionButtonTextView:


                dialogRegion.show();

                break;
            case R.id.buttonGemOnMatch:

                SecondActivity.ahBottomNavigation.setCurrentItem(3);

                break;
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
