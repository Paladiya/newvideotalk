package com.tech.rtcapp.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ShareCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.Constant;
import com.tech.rtcapp.Models.ModelUserData;
import com.tech.rtcapp.R;
import com.tech.rtcapp.SecondActivity;
import com.tech.rtcapp.SessionManager;
import com.tech.rtcapp.SignUpActivity;
import com.tech.rtcapp.Utiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    View view;
    TextView TvName, TvCountry, TvId, TvCoin, TvGender;
    FloatingActionButton FlImage;
    CircleImageView IvProfile;
    ModelUserData modelUserData;
    CollapsingToolbarLayout collapsingToolbar;
    String path = Environment.getExternalStorageDirectory().toString()
            + "/Pictures/" + System.currentTimeMillis() + ".png";
    File imageFile;
    AlertDialog.Builder builder;
    String name;
    APIInterface apiInterface;
    RelativeLayout RelShare, RelFaq, RelRate, RelFeed, RelLogout;
    Intent intent;
    Context mContext;
    TextView TvShareCoin, TvRateCoin;
    SessionManager sessionManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        Utiles.ShowProgres(mContext,getString(R.string.loading));
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sessionManager = new SessionManager(getActivity());

        modelUserData = ((SecondActivity) mContext).sessionManager.getUserDetails();
//        Log.d("coin",modelUserData.getCoin());
        collapsingToolbar = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing);
//        collapsingToolbar.setTitle("Your Title");
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.transperent)); // transperent color = #00000000
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.solid_white)); //Color of your title

        FlImage = (FloatingActionButton) view.findViewById(R.id.btn_profile);

        TvName = (TextView) view.findViewById(R.id.nameTextView);
        TvName.setText(modelUserData.getName());
        TvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGetName();
            }
        });

        TvGender = (TextView) view.findViewById(R.id.tv_gender);
        TvGender.setText(modelUserData.getGender());

        TvCountry = (TextView) view.findViewById(R.id.locationInfoView);
        if (modelUserData.getCountry() !=null){
            TvCountry.setText(modelUserData.getCountry());
        }else {
            ((LinearLayout)view.findViewById(R.id.myLocationView)).setVisibility(View.GONE);
        }


        TvCoin = (TextView) view.findViewById(R.id.tv_coin);
        TvCoin.setText(String.valueOf(modelUserData.getCoin()));

        TvId = (TextView) view.findViewById(R.id.tv_id);
        TvId.setText(modelUserData.getEmail().substring(0, modelUserData.getEmail().indexOf("@")));

        RelFaq = (RelativeLayout) view.findViewById(R.id.rel_faq);
        RelFeed = (RelativeLayout) view.findViewById(R.id.rel_feedback);
        RelLogout = (RelativeLayout) view.findViewById(R.id.rel_logout);
        RelRate = (RelativeLayout) view.findViewById(R.id.rel_rateus);
        RelShare = (RelativeLayout) view.findViewById(R.id.rel_share);

        RelShare.setOnClickListener(this);
        RelFaq.setOnClickListener(this);
        RelFeed.setOnClickListener(this);
        RelRate.setOnClickListener(this);
        RelLogout.setOnClickListener(this);

        TvShareCoin = (TextView)view.findViewById(R.id.tv_share_coin);
        TvRateCoin = (TextView)view.findViewById(R.id.tv_rate_coin);

        TvShareCoin.setText(String.valueOf(getResources().getInteger(R.integer.share_coin)));
        TvShareCoin.setText(String.valueOf(getResources().getInteger(R.integer.rate_coin)));

        TvShareCoin.setVisibility(View.GONE);
        TvRateCoin.setVisibility(View.GONE);

        IvProfile = (CircleImageView) view.findViewById(R.id.iv_profile);
        FlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
                startActivityForResult(intent, 1213);
            }
        });
        if (modelUserData.getImage() != null) {
            try {
                IvProfile.setImageURI(Uri.parse(modelUserData.getImage()));

            } catch (Exception e) {
                IvProfile.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));

            }
        } else {
            IvProfile.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));
        }

        if (modelUserData.getImage() != null) {
            try {
                FlImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera_normal));

            } catch (Exception e) {
                FlImage.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));

            }
        } else {
            FlImage.setImageDrawable(modelUserData.getGender().equals(Constant.MALE) ? getResources().getDrawable(R.drawable.img_male) : getResources().getDrawable(R.drawable.img_female));
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utiles.DismissProgress();
    }

    private void showGetName() {

        builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getString(R.string.inputNameTitle));
        final EditText input = new EditText(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        input.setLayoutParams(params);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name = input.getText().toString();
                if (name.equals(" ")) {
                    Toast.makeText(mContext, getString(R.string.pls_entvalid_name), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    return;
                }
                dialog.dismiss();
                Log.d("name", name);
                Utiles.ShowProgres(mContext, getString(R.string.plswaite));
                apiInterface.UpdateUserName(modelUserData.get_id(), name).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        Utiles.DismissProgress();
                        if (response.code() == 200) {
                            Toast.makeText(mContext, getString(R.string.name_update_suc), Toast.LENGTH_SHORT).show();
                            modelUserData.setName(name);
                            ((SecondActivity) mContext).sessionManager.updateUser(modelUserData);
                            TvName.setText(name);
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Utiles.DismissProgress();
                        Toast.makeText(mContext, getString(R.string.server_maintanance), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onprofile", "activityresult");
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            IvProfile.setImageBitmap(selectedImage);
            OutputStream out = null;
            imageFile = new File(path);
            try {
                out = new FileOutputStream(imageFile);
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }

                } catch (Exception exc) {
                }
            }
            modelUserData.setImage(path);
            ((SecondActivity) mContext).sessionManager.updateUser(modelUserData);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_share:

                intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
//                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(outputFile));
                intent.putExtra(Intent.EXTRA_TEXT, Constant.app_link + mContext.getPackageName());
                intent.setType("text/plain");
                startActivity(intent);
                modelUserData.setCoin(modelUserData.getCoin()+getResources().getInteger(R.integer.share_coin));
                apiInterface.UpdateUserCoin(modelUserData.getJWT(), modelUserData.getCoin()).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.code() == 200) {
                            sessionManager.updateUser(modelUserData);
                            TvCoin.setText(String.valueOf(modelUserData.getCoin()));
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });
                break;
            case R.id.rel_faq:

                intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(getString(R.string.privacylink)));
                startActivity(intent);

                break;
            case R.id.rel_rateus:

                Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName()));
                startActivity(rateIntent);
                modelUserData.setCoin(modelUserData.getCoin()+getResources().getInteger(R.integer.rate_coin));
                apiInterface.UpdateUserCoin(modelUserData.getJWT(), modelUserData.getCoin()).enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.code() == 200) {
                            sessionManager.updateUser(modelUserData);
                            TvCoin.setText(String.valueOf(modelUserData.getCoin()));
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });

                break;
            case R.id.rel_feedback:

                ShareCompat.IntentBuilder.from(getActivity())
                        .setType("message/rfc822")
                        .addEmailTo(getString(R.string.adminmain))
                        .setSubject(getString(R.string.app_name))
//                        .setText()
                        //.setHtmlText(body) //If you are using HTML in your body text
                        .setChooserTitle(getString(R.string.sendmail))
                        .startChooser();

                break;
            case R.id.rel_logout:

//                ((SecondActivity) mContext).sessionManager.setLogin(false);
//                ((SecondActivity) mContext).sessionManager.updateUser(new ModelUserData());
                ((SecondActivity) mContext).sessionManager.clearAll();
                intent = new Intent(getActivity(), SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

                break;
        }
    }
}
