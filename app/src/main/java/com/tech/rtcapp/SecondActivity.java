package com.tech.rtcapp;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.zxing.common.StringUtils;
import com.tech.rtcapp.API.APIClient;
import com.tech.rtcapp.API.APIInterface;
import com.tech.rtcapp.API.SocketConnection;
import com.tech.rtcapp.Fragments.CoinFragment;
import com.tech.rtcapp.Fragments.ExploreFragment;
import com.tech.rtcapp.Fragments.HistoryFragment;
import com.tech.rtcapp.Fragments.ProfileFragment;
import com.tech.rtcapp.Models.ModelBuyCoins;
import com.tech.rtcapp.Models.ModelPurchase;
import com.tech.rtcapp.Models.ModelUserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import am.appwise.components.ni.NoInternetDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SecondActivity extends AppCompatActivity implements View.OnClickListener, ExploreFragment.OnFragmentInteractionListener, BillingProcessor.IBillingHandler {


    public static AHBottomNavigation ahBottomNavigation;
    FrameLayout frameLayout;
    FragmentManager fragmentManager;
    APIInterface apiInterface;
    public SessionManager sessionManager;
    Intent intent;
    ModelUserData modelUserData;
    LocationManager locationManager;
    String country;
    GoogleApiClient googleApiClient;
    Dialog dialogExit;
    private TextView yes, no;
    LinearLayout linearLayoutAds;
    boolean doubleBackToExitPressedOnce = false;
    NoInternetDialog noInternetDialog;
    int currentPosition = -1;
    String newToken;
    public BillingProcessor bp;
    ArrayList<ModelBuyCoins> coinsModelArrayList;
    String gemId;
    ModelPurchase purchaseModel;
    CoinFragment coinFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_second_view);
//        toolbar = (Toolbarbottom_navigation) findViewById(R.id.toolbar);
//        toolbar.setTitle(R.string.app_name);
//        setSupportActionBar(toolbar);
//        Utiles.showInterstitialAd(this);

        noInternetDialog = new NoInternetDialog.Builder(this)
                .setBgGradientStart(getResources().getColor(R.color.colorPrimaryDarktoo)) // Start color for background gradient
                .setBgGradientCenter(getResources().getColor(R.color.colorPrimaryDark)) // Center color for background gradient
                .setBgGradientEnd(getResources().getColor(R.color.colorPrimary)) // End color for background gradient
                .setCancelable(false).build();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        fragmentManager = getSupportFragmentManager();
        sessionManager = new SessionManager(this);
        Log.d("login",String.valueOf(sessionManager.isLoggedIn()));

        bp = new BillingProcessor(this, getString(R.string.billingProcessorKey), this);
        bp.initialize();

        if (!sessionManager.isLoggedIn()) {
            intent = new Intent(SecondActivity.this, SignUpActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return;
        }
        Log.d("gen",sessionManager.getUserDetails().getGender());
        if (sessionManager.getUserDetails().getGender() == null) {
            intent = new Intent(SecondActivity.this, SignUpExternalActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }


        SocketConnection.connect(this);

        updateFcmToken();

        frameLayout = (FrameLayout) findViewById(R.id.fragment_container);

        ahBottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.explore, R.drawable.explore, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.histoy, R.drawable.hitory, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.profile, R.drawable.profile, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.coins, R.drawable.coins_icon2, R.color.colorPrimary);

        ahBottomNavigation.setColored(true);
        ahBottomNavigation.setTitleTextSize(35, 30);
        ahBottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        coinFragment = new CoinFragment();

        ahBottomNavigation.addItem(item1);
        ahBottomNavigation.addItem(item2);
        ahBottomNavigation.addItem(item3);
        ahBottomNavigation.addItem(item4);
        ahBottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (ExploreFragment.isRunning && position!=0){
                    ExploreFragment.countDownTimer.cancel();
                    ExploreFragment.isRunning = false;
                }
                if (currentPosition==position){
                    return false;
                }
                currentPosition = position;
                    switch (position) {
                    case 0:
                        fragmentManager.beginTransaction().replace(R.id.fragment_container, new ExploreFragment()).commit();
                        break;
                    case 1:
                        fragmentManager.beginTransaction().replace(R.id.fragment_container, new HistoryFragment()).commit();
                        break;
                    case 2:
                        fragmentManager.beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
                        break;
                    case 3:
                        fragmentManager.beginTransaction().replace(R.id.fragment_container, coinFragment).commit();
                        break;
                }
                // Do something cool here...
                return true;
            }
        });




        dialogExit = new Dialog(this, R.style.WideDialog);
        dialogExit.setContentView(R.layout.activity_exit);
        dialogExit.setCancelable(false);
        this.yes = (TextView) dialogExit.findViewById(R.id.btnYes);
        this.no = (TextView) dialogExit.findViewById(R.id.btnNo);
        this.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
                finish();
            }
        });
        this.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
            }
        });
        linearLayoutAds = (LinearLayout) dialogExit.findViewById(R.id.lad);
        if (Utiles.isInternetOn(this)) {
            linearLayoutAds.addView(Utiles.GetAdviewBig(this));

        } else {
            linearLayoutAds.setBackgroundResource(R.drawable.logo);
        }

//        if (!Utiles.isInternetOn(this))
//        {
//            noInternetDialog.show();
//            Log.d("nointernet","yes");
//            return;
//        }
        if(checkPermissons(Constant.GOOGLE_PERMISSION_CODE)){
            ahBottomNavigation.setCurrentItem(0);
        }

    }

    public void updateFcmToken() {
        modelUserData = sessionManager.getUserDetails();

        if(modelUserData!=null){
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    newToken = task.getResult().getToken();
                    Log.d("newtoken",newToken);
                    if(!TextUtils.equals(newToken,modelUserData.getFcmToken())){
                        Log.d("token","notmatch");
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(Constant.TOKEN, newToken);
                            SocketConnection.connect(SecondActivity.this).emit(Constant.SETTOKEN, jsonObject);
                            modelUserData.setFcmToken(newToken);
                            sessionManager.updateUser(modelUserData);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        Log.d("token","match");
                    }
                }
            });
        }

    }

    private boolean checkPermissons(int PermissionCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        PermissionCode);
                return false;
            }
            return true;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utiles.isInternetOn(this))
        {
            noInternetDialog.show();
            Log.d("nointernet","yes");
            return;
        }
        modelUserData = sessionManager.getUserDetails();
        if (modelUserData.getCountry() == null || modelUserData.getCountry().equals("")) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        Constant.PERMISSION_LOCATION_CODE);
                Log.d("permission", "not granted");
                return;
            } else {
                GetAndUpdateLocation();
            }
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constant.ISAVAILABLE, true);
            SocketConnection.connect(SecondActivity.this).emit(Constant.SETAVAILABLE, jsonObject);
            modelUserData.setFcmToken(newToken);
            modelUserData.setAvailable(true);
            sessionManager.updateUser(modelUserData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean allGranted = false;
        if (requestCode == Constant.PERMISSION_LOCATION_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true;
                } else {
                    allGranted = false;
                }
            }
            if (!allGranted) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        Constant.PERMISSION_LOCATION_CODE);
            } else {
                GetAndUpdateLocation();
            }
        }else if (requestCode == Constant.GOOGLE_PERMISSION_CODE){
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true;
                } else {
                    allGranted = false;
                }
            }
            if (!allGranted) {
                checkPermissons(Constant.GOOGLE_PERMISSION_CODE);
            } else {
                ahBottomNavigation.setCurrentItem(0);
            }
        }
    }

    private void GetAndUpdateLocation() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice()) {
            Toast.makeText(SecondActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
            enableLoc();
            return;
        } else {
            getLocation();
        }
    }

    private boolean hasGPSDevice() {

        if (locationManager == null)
            return false;
        final List<String> providers = locationManager.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void getLocation() {

        Log.d("get location", "click");

        SingleShotLocationProvider.requestSingleUpdate(SecondActivity.this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Geocoder gcd = new Geocoder(SecondActivity.this, Locale.getDefault());
                        List<Address> addresses;
                        try {
                            addresses = gcd.getFromLocation(location.latitude, location.longitude, 1);
                            country = addresses.get(0).getCountryName();
                            Log.d("Location", "my location is " + country);

                            apiInterface.UpdateCountry(modelUserData.get_id(), country).enqueue(new Callback<Object>() {
                                @Override
                                public void onResponse(Call<Object> call, Response<Object> response) {
                                    if (response.code() == 200) {
                                        modelUserData.setCountry(country);
                                        sessionManager.updateUser(modelUserData);
                                    }
                                }

                                @Override
                                public void onFailure(Call<Object> call, Throwable t) {

                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    private void enableLoc() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        googleApiClient.connect();
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                    }
                })
                .build();

        googleApiClient.connect();

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        final LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest).setAlwaysShow(true).build();

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest);
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes
                            .RESOLUTION_REQUIRED: {
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SecondActivity.this, Constant.GOOGLE_LOCATION_CODE);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }




//    @Override
//    protected void onResume() {
//        super.onResume();
//        Log.d(TAG, "coins " + String.valueOf(sessionManager.getUserDetails().getCoin()));
//        btn_coin.setText(String.valueOf(sessionManager.getUserDetails().getCoin()));
//        if (isRunning) {
//            countDownTimer.start();
//        }
//        camera = Camera.open(1);
//        showCamera = new ShowCamera(this, camera);
//        preview.removeAllViews();
//        preview.addView(showCamera);
//
//    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        Log.d(TAG,"onrestart");
//        fillData();
//    }


    @Override
    protected void onDestroy() {
        Log.d(getClass().getSimpleName(),"ondestroy");
        if (SocketConnection.socket!=null){
            SocketConnection.connect(this).disconnect();
        }
        if (bp != null) {
            bp.release();
        }
        noInternetDialog.onDestroy();
        super.onDestroy();

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        Log.d("onactivityresult",getClass().getSimpleName());
//        Log.d("onactivityresult_code",String.valueOf(requestCode));
//        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
//            Log.d("onactivityresult","called");
//        }
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments())
        {
            if (fragment != null)
            {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (ahBottomNavigation.getCurrentItem() != 0) {
            ahBottomNavigation.setCurrentItem(0);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else {
            if (Utiles.isInternetOn(this)) {
                dialogExit.show();
            } else {
                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        Log.d("onbilling","purchased "+details.purchaseInfo);
        if(bp.consumePurchase(productId)){
            for (int i = 0; i < coinFragment.coinsModelArrayList.size(); i++) {
                if (coinFragment.coinsModelArrayList.get(i).getSubId().equals(productId)) {
//Do whatever you want here
                    gemId = coinFragment.coinsModelArrayList.get(i).get_id();
                }
            }
            purchaseModel = new ModelPurchase();
            purchaseModel.setToken(details.purchaseToken);
            purchaseModel.setSignature(details.purchaseInfo.signature);
            purchaseModel.setOrderId(details.orderId);
            purchaseModel.setGemsId(gemId);
            purchaseModel.setUserId(coinFragment.userModel.get_id());
            purchaseModel.setPackageName(details.purchaseInfo.purchaseData.packageName);
            coinFragment.RegisterPackage();
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.d("onbilling","onPurchaseHistoryRestored");

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        Log.d("onbilling","error called ");

    }

    @Override
    public void onBillingInitialized() {
        Log.d("onbilling","initialized");
    }


}
