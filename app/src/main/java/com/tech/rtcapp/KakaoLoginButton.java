package com.tech.rtcapp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class KakaoLoginButton extends RelativeLayout {
    /* renamed from: a */
    private View f5402a;
    /* renamed from: b */
    private TextView f5403b;

    public KakaoLoginButton(Context context) {
        this(context, null);
    }

    public KakaoLoginButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public KakaoLoginButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m6319a();
    }

    /* renamed from: a */
    private void m6319a() {
        inflate(getContext(), R.layout.kakao_login_layout_azar, this);
        this.f5402a = findViewById(R.id.kakao_login_button_iconview);
        this.f5403b = (TextView) findViewById(R.id.kakao_login_button_textview);
        setBackgroundResource(R.drawable.selector_btn_bg_signup_kakao);
    }

    public View getIconView() {
        return this.f5402a;
    }

//    public <TV extends TextView> TV getLabelView() {
//        return this.f5403b;
//    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        this.f5402a.setVisibility(i);
        this.f5403b.setVisibility(i);
    }
}
