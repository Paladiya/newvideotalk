package com.tech.rtcapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnKeyListener;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.tt.whorlviewlibrary.WhorlView;

import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

public class Utiles {

    public static final String ShareName = "Live";
    public static final String SKU = "SKU";
    static SharedPreferences sharedPreferences;
    static Dialog progressDialog;

    public  Utiles(Context context)
    {
        sharedPreferences = context.getSharedPreferences(ShareName,MODE_PRIVATE);
    }

    public static void SaveFirstValue(String name,Boolean values)
    {
        sharedPreferences.edit().putBoolean(name,values).apply();
    }

    public  static Boolean GetFirstValue(String key)
    {
        return  sharedPreferences.getBoolean(key,true);
    }


    public static void SaveSKU(String values)
    {
        sharedPreferences.edit().putString(SKU,values).apply();
    }

    public static String GetSKU()
    {
        return  sharedPreferences.getString(SKU," ");
    }



    public static void SaveDialogShow(String name, Boolean values) {
        sharedPreferences.edit().putBoolean(name, values).apply();
    }

    public static Boolean GetDialogShow(String key) {
        return sharedPreferences.getBoolean(key, false);
    }


    static class DialogInterface implements OnKeyListener{
        DialogInterface() {
        }

        @Override
        public boolean onKey(android.content.DialogInterface dialog, int keyCode, KeyEvent event) {
            return keyCode == 4;
        }
    }

    public static ProgressDialog getProgressDialog(String str, Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnKeyListener(new DialogInterface());
        progressDialog.setMessage(str);
        return progressDialog;
    }

    public static InterstitialAd mInterstitialAd;

    public static void showInterstitialAd(final Context context)
    {
        mInterstitialAd=new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getResources().getString(R.string.admob_interstitial));

        AdRequest adRequest = new AdRequest.Builder().addTestDevice("553BA7A9C0A22C16AC4F04E02467018E")
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial(context);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d("interstialad code = ",String.valueOf(i));
            }
        });
    }

    public static void showInterstitial(Context context) {
        if (mInterstitialAd.isLoaded()) {
            SKU sku = new SKU();
            String value = GetSKU();
            if (!value.equals(" "))
            {
                sku = new Gson().fromJson(GetSKU(), com.tech.rtcapp.SKU.class);
                Calendar current = Calendar.getInstance();
                Calendar last  = Calendar.getInstance();
                last.setTimeInMillis(Long.valueOf(sku.getGetSku_time_end()));
                if (current.getTimeInMillis()>last.getTimeInMillis())
                {
                    mInterstitialAd.show();
                }
            }
        }
    }

    public static AdView GetAdview(final Context context)
    {
        AdView adView=new AdView(context);

        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(context.getResources().getString(R.string.admob_banner));
//        553BA7A9C0A22C16AC4F04E02467018E
        AdRequest adRequest=new AdRequest.Builder().addTestDevice("553BA7A9C0A22C16AC4F04E02467018E")
                .build();

        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Toast.makeText(context, "Ad is loaded!", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                //Toast.makeText(context, "Ad is closed!", //Toast.LENGTH_SHORT).show();
                //Toast.makeText(context, "Ad is opend!", //Toast.LENGTH_SHORT).show();
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        GetAdview(context);
                    }
                },1);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(context, "Ad failed to load! error code: " + errorCode, //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                //Toast.makeText(context, "Ad left application!", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();

            }
        });

        return adView;
    }

    public static AdView GetAdviewBig(final Context context)
    {
        AdView adView=new AdView(context);
        adView.setAdSize(AdSize.LARGE_BANNER);
        adView.setAdUnitId(context.getResources().getString(R.string.admob_banner));
        AdRequest adRequest=new AdRequest.Builder().addTestDevice("553BA7A9C0A22C16AC4F04E02467018E")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Toast.makeText(context, "Ad is loaded!", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                //Toast.makeText(context, "Ad is closed!", //Toast.LENGTH_SHORT).show();
                //Toast.makeText(context, "Ad is opend!", //Toast.LENGTH_SHORT).show();
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        GetAdview(context);
                    }
                },1);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(context, "Ad failed to load! error code: " + errorCode, //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                //Toast.makeText(context, "Ad left application!", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();

            }
        });

        return adView;
    }

    public static boolean isInternetOn(Context context) {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }

    public static void ShowProgres(Context context,String message)
    {
        progressDialog = new Dialog(context);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.progress_layout, null);
        progressDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.setContentView(dialogView);
        progressDialog.setCancelable(false);
        ((WhorlView)progressDialog.findViewById(R.id.whorl2)).start();
        ((TextView)progressDialog.findViewById(R.id.dialogText)).setText(message);
        progressDialog.show();
    }

    public static void DismissProgress()
    {
        if (progressDialog!=null)
        {
            progressDialog.dismiss();
        }
    }

    public static void showToast(Context context,String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
