package com.tech.rtcapp.API;

import android.content.Context;
import android.util.Log;

import com.tech.rtcapp.API.Links;
import com.tech.rtcapp.Constant;
import com.tech.rtcapp.MyApplication;
import com.tech.rtcapp.SessionManager;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class  SocketConnection {

    public static Socket socket;
    public static SessionManager sessionManager;
    public static IO.Options options;
    public static boolean SocketConnected = false;

//    static {
//        Log.d("static","call");
//    }

    public static synchronized  Socket connect(Context context) {

        try {
            if (socket == null){
                sessionManager = new SessionManager(context);
                options = new IO.Options();
                options.forceNew = true;
                options.reconnection = false;
                options.multiplex = false;
                options.query = "auth_token=" + sessionManager.getUserDetails().getJWT() + "&" + Constant.UserId + "=" + sessionManager.getUserDetails().get_id(); //+ stringBuilder.toString() ;
                Log.d("socket", "Connected" + String.valueOf(options.query));
                socket = IO.socket(Links.URL, options);
                socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d("socket", "EVENT_DISCONNECT");
                        SocketConnected = false;
                    }
                });
                socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d("socket", "EVENT_CONNECT");
                        SocketConnected = true;
                    }
                });
                socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d("socket", "EVENT_ERROR");
                    }
                });
                socket.on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d("socket", "EVENT_MESSAGE");
                    }
                });
                socket.connect();
            }else {
                if (!socket.connected()){
                    socket.connect();
                }
            }

//            if (!SocketConnected) {
//                Log.d("socket","connect");
//                sessionManager = new SessionManager(context);
//                options = new IO.Options();
//                options.forceNew = false;
//                options.reconnection = false;
//                options.multiplex = false;
//                options.query = "auth_token=" + sessionManager.getUserDetails().getJWT() + "&" + Constant.UserId + "=" + sessionManager.getUserDetails().get_id(); //+ stringBuilder.toString() ;
//                Log.d("socket", "Connected" + String.valueOf(options.query));
//                socket = IO.socket(Links.URL, options);
//                socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Log.d("socket", "EVENT_DISCONNECT");
//                        SocketConnected = false;
//                    }
//                });
//                socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Log.d("socket", "EVENT_CONNECT");
//                        SocketConnected = true;
//                    }
//                });
//                socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Log.d("socket", "EVENT_ERROR");
//                    }
//                });
//                socket.connect();
//            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return socket;
    }


}
