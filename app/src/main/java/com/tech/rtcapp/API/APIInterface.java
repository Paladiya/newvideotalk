package com.tech.rtcapp.API;

import com.tech.rtcapp.Models.ModelHistory;
import com.tech.rtcapp.Models.ModelPurchase;
import com.tech.rtcapp.Models.ModelUserData;
import com.tech.rtcapp.Models.ModelBuyCoins;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("login")
    Call<Object> Login(@Query("email")String email, @Query("password")String password);

    @POST("signup")
    Call<Object> SignUp(@Body ModelUserData modelUserData);

    @GET("getCoinsList")
    Call<ArrayList<ModelBuyCoins>> GetGemsList();

    @GET("getCountryList")
    Call<Object> GetCountryList();

    @POST("getHistoryList")
    Call<ArrayList<ModelHistory>> GetHistoryList(@Query("userId") String userId);

    @POST("updateUser")
    Call<ModelUserData> UpdateUser(@Body ModelUserData modelUserData);

    @POST("registerPackage")
    Call<ModelUserData> RegisterPackage(@Body ModelPurchase modelPurchase);

    @GET("getUser")
    Call<ModelUserData> GetUser(@Query("id") String id);

    @POST("updateCoin")
    Call<Object> UpdateUserCoin(@Query("jwt") String jwt,@Query("coin") int value);

    @POST("updateName")
    Call<Object> UpdateUserName(@Query("userId") String id,@Query("name") String name);

    @POST("updateGender")
    Call<Object> UpdateGender(@Query("userid")String userId,@Query("gender")String gender, @Query("birthdate")String birthDate);

    @POST("updateCountry")
    Call<Object> UpdateCountry(@Query("userid")String userId,@Query("country")String country);

    @POST("callRequest")
    Call<Object> CallRequest(@Query("userId")String userId,@Query("myId")String myId);


}
